import ReactNative,{NativeModules} from 'react-native';
import I18n from 'react-native-i18n';
// Import all locales
import en from './en.json';
import ar from './ar.json';

// Should the app fallback to English if user locale doesn't exists
I18n.fallbacks = true;

// Define the supported translations
I18n.translations = {
  en,
  ar
};


// I18n.locale = I18n.currentLocale();
if (NativeModules.I18nManager) {
    const {localeIdentifier} = NativeModules.I18nManager;
    const isRTL = localeIdentifier.indexOf('ar') === 0;
    I18n.locale = localeIdentifier.substring(0, 2);
    ReactNative.I18nManager.allowRTL(false);
}

export function strings(name, params = {}) {
  return I18n.t(name, params);
};

export default I18n;