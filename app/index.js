import { Navigation } from 'react-native-navigation';
import { Provider } from 'react-redux';
import {colors} from './configs/Colors.js';
import { registerScreens } from './screens';
import configureStore from './configs/configureStore';
import { persistStore } from 'redux-persist';


const store = configureStore();

const navigatorStyle = {
  statusBarColor: colors.white,
  navBarTextFontFamily: 'Dosis-Medium',
  navBarTextFontSize: 30,
  statusBarTextColorScheme: 'dark',
  navigationBarColor: colors.white,
  navBarBackgroundColor: colors.white,
  navBarTextColor: colors.primary,
  navBarButtonColor: colors.secondry,
  tabBarButtonColor: 'red',
  screenBackgroundColor: colors.white,
  tabBarSelectedButtonColor: 'red',
  tabBarBackgroundColor: 'white',
  topBarElevationShadowEnabled: false,
  navBarHideOnScroll: false,
  tabBarHidden: true,
  drawUnderTabBar: true,
  navBarTitleTextCentered: true
};
    persistStore(store, null, () => {
    registerScreens(store, Provider)

  Navigation.startSingleScreenApp({
        screen: {
                    screen: 'Home', // unique ID registered with Navigation.registerScreen
                    title: 'Tstore', // title of the screen as appears in the nav bar (optional)
                    navigatorStyle, // override the navigator style for the screen, see "Styling the navigator" below (optional)
                    rightButtons: [{
                                    // id: 'cart',
                                     //component: 'CartCar',
                                  }],
                    leftButtons: [{
                              				id: 'sideMenu',
                              				icon: require('./images/menu.png')
                                 }]
                                 },
                    drawer: {
                                       left: {
                                              screen: 'Drawer'
                                             }
                            },
                    //passProps: {my_persistor: persistor}

                                  });
});
 /* Navigation.startTabBasedApp({
    tabs: [
      {
        label: 'One',
        screen: 'Screen1',
        icon: require('./images/icon1.png'),
        selectedIcon: require('./images/icon1_selected.png'),
        title: 'Screen One'
      },
      {
        label: 'Two',
        screen: 'Screen2',
        icon: require('./images/icon2.png'),
        selectedIcon: require('./images/icon2_selected.png'),
        title: 'Screen Two'
      }
    ]
  });*/
