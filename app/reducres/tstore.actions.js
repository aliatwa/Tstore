import * as types from './actionTypes';

// This is an action creator, it simply specifies the action.
// this is what we call to send an action.
export function plusQuantity(data) {
  return {
    type: types.PLUS_QUANTITY,
    id: data,
    quantity: 0,
  }
}
export function minusQuantity(data) {
  return {
    type: types.MINUS_QUANTITY,
    id: data,
    quantity:0,

  }}
export function addItem(data) {
  return {
    type: types.ADD_ITEM,
    payload: data
  }
}
export function removeItem(data) {
  return {
    type: types.REMOVE_ITEM,
    payload: data
  }
}
export function resetCart() {
  return {
    type: types.RESET_CART
  }
}
export function placeOrder(data) {
  return {
    type: types.PLACE_ORDER,
    payload: data
  }
}
export function removeOrder(data) {
  return {
    type: types.REMOVE_ORDER,
    payload: data
  }
}
export function addSaved(data) {
  return {
    type: types.ADD_SAVED,
    payload: data
  }
}
export function removeSaved(data) {
  return {
    type: types.REMOVE_SAVED,
    payload: data
  }
}
export function setLocation(lat,lon) {
  return {
    type: types.SET_LOCATION,
    lat: lat,
    lon: lon,
  }
}
export function updateProfile(data) {
  return {
    type: types.UPDATE_PROFILE,
    payload: data
  }
}