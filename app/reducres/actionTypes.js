export const RETRIEVE_PRODUCT_DETAILS_SUCCESS = 'RETRIEVE_PRODUCT_DETAILS_SUCCESS';
export const PLACE_ORDER = 'PLACE_ORDER';
export const REMOVE_ORDER = 'REMOVE_ORDER';
export const RETRIEVE_STORE_DETAILS_SUCCESS = 'RETRIEVE_STORE_DETAILS_SUCCESS';
export const ADD_ITEM = 'ADD_ITEM';
export const ADD_SAVED = 'ADD_SAVED';
export const SET_LOCATION = 'SET_LOCATION';
export const UPDATE_PROFILE = 'UPDATE_PROFILE';
export const REMOVE_SAVED = 'REMOVE_SAVED';
export const PLUS_QUANTITY = 'PLUS_QUANTITY';
export const MINUS_QUANTITY = 'MINUS_QUANTITY';
export const REMOVE_ITEM = 'REMOVE_ITEM';
export const RESET_CART = 'RESET_CART';