export default {
	tstore: {
		details: {},
		orders: [],
		store: {},
		cart: [],
		saved: [],
		profile: {
			location: [],
			name: null,
			signedin: false,
			gender: null,
			address: null,
			gov: null,
			phone: null
		}
	}
};