import * as types from './actionTypes';
import initialState from './initialState';

export default function (state = initialState.tstore, action) {
	switch (action.type) {

		case types.RETRIEVE_PRODUCT_DETAILS_SUCCESS:
			return {
				...state,
				productDetails: state.productDetails
	};
		case types.RETRIEVE_STORE_DETAILS_SUCCESS:
			return {
				...state,
				storeDetails: state.storeDetails
	};
		case types.PLUS_QUANTITY:
		    return {
                ...state,
                cart: state.cart.map(item => 
                item.tmpId === action.id ? { ...item, quantity: item.quantity+1 } : item
        ) 
    };
		case types.MINUS_QUANTITY:
		    return {
                ...state,
                cart: state.cart.map(item => 
                item.tmpId === action.id ? { ...item, quantity: item.quantity-1 } : item
        ) 
    };
		case types.ADD_ITEM:
			return {
				...state,
				cart: [...state.cart, action.payload]
	};
		case types.REMOVE_ITEM:
			return {
				...state,
				cart: state.cart.filter(item => item !== action.payload),
	};
		case types.RESET_CART:
			return {
				...state,
				cart: []
	};
		case types.PLACE_ORDER:
			return {
				...state,
				orders: [...state.orders, action.payload]
	};
		case types.REMOVE_ORDER:
			return {
				...state,
				orders: state.orders.filter(item => item.id !== action.payload),
	};
		case types.ADD_SAVED:
			return {
				...state,
				saved: [...state.saved, action.payload]
	};
		case types.REMOVE_SAVED:
			return {
				...state,
				saved: state.saved.filter(item => item.id !== action.payload),
	};
		case types.SET_LOCATION:
			return {
				...state,
				profile: {
					...state.profile,
					location: [action.lat,action.lon]
				}
	};
		case types.UPDATE_PROFILE:
			return {
				...state,
				profile: {
					...state.profile,
					name: action.payload.name,
					phone: action.payload.phone,
					address: action.payload.address,
					gender: action.payload.gender,
					gov: action.payload.gov,
				}
	};
		default:
			return state;
	}
}