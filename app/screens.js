import { Navigation } from 'react-native-navigation';

import Home from './screens/Home';
import Drawer from './screens/Drawer';
import Profile from './screens/Profile';
import ProductDetails from './screens/ProductDetails';
import Products from './screens/Products';
import Cart from './screens/Cart';
import SearchProduct from './screens/SearchProduct';

import Brand from './screens/Brand';
import Brands from './screens/Brands';
import ProductMini from './components/ProductMini';

import Store from './screens/Store';
import Stores from './screens/Stores';
import Login from './screens/Login';
import ResetPassword from './screens/ResetPassword';
import Signup from './screens/Signup';
import AllDone from './screens/AllDone';
import Confirm from './screens/Confirm';
import Feedback from './screens/Feedback';
import ChangeAccount from './screens/ChangeAccount';
import PersonalInfo from './screens/PersonalInfo';
import OrderInfo from './screens/OrderInfo';
import Category from './screens/Category';

import CartCar from './components/CartCar';

export function registerScreens(store, Provider) {
  Navigation.registerComponent('Home', () => Home, store, Provider);
  Navigation.registerComponent('Drawer', () => Drawer, store, Provider);
  Navigation.registerComponent('Profile', () => Profile, store, Provider);
  Navigation.registerComponent('Cart', () => Cart, store, Provider);
  Navigation.registerComponent('Confirm', () => Confirm, store, Provider);
  Navigation.registerComponent('Feedback', () => Feedback, store, Provider);
  Navigation.registerComponent('ChangeAccount', () => ChangeAccount, store, Provider);
  Navigation.registerComponent('PersonalInfo', () => PersonalInfo, store, Provider);
  Navigation.registerComponent('OrderInfo', () => OrderInfo, store, Provider);
  Navigation.registerComponent('Category', () => Category, store, Provider);

  Navigation.registerComponent('ProductDetails', () => ProductDetails, store, Provider);
  Navigation.registerComponent('Products', () => Products, store, Provider);
  Navigation.registerComponent('SearchProduct', () => SearchProduct, store, Provider);

  Navigation.registerComponent('Store', () => Store, store, Provider);
  Navigation.registerComponent('Stores', () => Stores, store, Provider);
  Navigation.registerComponent('Brand', () => Brand, store, Provider);
  Navigation.registerComponent('Brands', () => Brands, store, Provider);
  Navigation.registerComponent('ProductMini', () => ProductMini, store, Provider);

  Navigation.registerComponent('Login', () => Login, store, Provider);
  Navigation.registerComponent('Signup', () => Signup, store, Provider);
  Navigation.registerComponent('ResetPassword', () => ResetPassword, store, Provider);
  Navigation.registerComponent('CartCar', () => CartCar, store, Provider);
  
  Navigation.registerComponent('AllDone', () => AllDone, store, Provider);
}