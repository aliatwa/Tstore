import React from 'react';
import {colors} from '../configs/Colors.js';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  ImageBackground,
  TouchableOpacity,ScrollView,
  View, StyleSheet, Text, Image
} from 'react-native';

const styles = StyleSheet.create({
  Container: {
    flexDirection:'row',
    justifyContent: 'flex-start',
    height: 150,
    marginHorizontal: 20,
  },
  removeorder: { 
    alignItems:'flex-start', 
    justifyContent:'center',
    marginHorizontal:10
  },
  removeordertext: { 
    alignItems:'flex-start', 
    justifyContent:'center',
    marginHorizontal:10,
    
  },
  imgcont: {
    alignItems:'center', 
    justifyContent:'center',
    marginLeft: 15,
  },
  orderimg: {
    borderRadius: 5,
    width: 125,
    height: 125,
  },
  infocont: {
    marginLeft: 25,
    marginVertical: 10,
    flex: 1,
    justifyContent: 'space-around',
    flexDirection: 'column',
    alignItems: 'flex-start', 
    },
  orderdate: {
    color: colors.ProductTitleDetails,
    fontFamily: "Dosis-Regular",
    fontSize: 17
    },
  productname: {
    color: colors.primary,
    fontFamily: "Dosis-SemiBold",
    fontSize: 20
    },
  totalprice: {
    color: colors.ProductTitleDetails,
    fontFamily: "Dosis-SemiBold",
    fontSize: 20
    },
  orderstatus: {
    color: colors.orderstatus,
    fontFamily: "Dosis-Regular",
    fontSize: 25
    },
  ordercont: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignSelf: 'center',
    alignItems: 'center',
    marginHorizontal: 10
    },
  canceltext: {
    color: colors.warning,
    fontFamily: "Dosis-SemiBold",
    fontSize: 17
  }
});

const Orders = ({ orders, viewOrder, removeOrder }) => (
  (orders.map((info) => {
    return(
    <View style={styles.Container} key={info.id}>
    {info.status == 'Pending' || info.status == 'Accepted' ? 
        <TouchableOpacity style={styles.removeordertext} onPress={removeOrder.bind(this, info.id)}>
          <Text style={styles.canceltext}>Cancel</Text>
        </TouchableOpacity>
    : <TouchableOpacity style={styles.removeorder} onPress={removeOrder.bind(this, info.id)}>
        <Icon name="remove-circle-outline" size={30} color={colors.remove_circle} />
      </TouchableOpacity>
    }
      <View style={styles.imgcont}>
         <Image style={styles.orderimg} source={{uri: info.backet[0].images[0].image}}/>
      </View>

      <View style={styles.infocont}>
         <Text style={styles.orderdate}>{info.orderdate}</Text>
         <View>
              <Text style={styles.productname}>
              {info.backet.length > 1 ? 'Multiple Items' : info.backet[0].title}</Text>
              <Text style={styles.totalprice}>{info.totalprice} EGP</Text>
         </View>
         { info.status == 'Pending' ?  <Text style={[styles.orderstatus, {color: colors.primary }]}>{info.status}</Text>
         : info.status == 'Accepted' ?  <Text style={[styles.orderstatus, {color: colors.orderstatus }]}>{info.status}</Text>
         : info.status == 'Picked' ?  <Text style={[styles.orderstatus, {color: colors.green }]}>{info.status}</Text>
         : <Text style={[styles.orderstatus, {color: colors.primary }]}>{info.status}</Text>
         }
        
      </View>

      <View style={styles.ordercont}>
          <TouchableOpacity style={styles.orderbtn} onPress={viewOrder.bind(this, info.id)}>
              <Icon name="keyboard-arrow-right" size={40} color={colors.ProductTitleDetails} />
          </TouchableOpacity>
      </View>
    </View>
    )}))
);

Orders.propTypes = {
  orders: PropTypes.array.isRequired,
  viewOrder: PropTypes.func.isRequired,
  removeOrder: PropTypes.func.isRequired,
};

export default Orders;