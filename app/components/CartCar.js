import React, { Component } from 'react';
import { Text, View, YellowBox,StyleSheet,Dimensions, Image, ScrollView, TouchableOpacity } from 'react-native';
import {colors} from '../configs/Colors.js';
import PropTypes from 'prop-types';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../reducres/tstore.actions';

const styles = StyleSheet.create({
  cart: {
    width: 50,
    height: 50,
    flex:1,
    right: 8,
    borderRadius: 45,
    justifyContent: 'center',
    alignItems: 'center',
},
  imgcon: {
    width: 45,
    height: 45,
    alignItems: 'center', //horizontal
    justifyContent: 'center',//vertical
},
  carimg: {
},
  cartnum: {
    color: colors.white,
    fontSize: 15,
    fontFamily: "Dosis-Medium", 
    borderRadius: 45,
    backgroundColor: colors.primary,
    textAlign: 'center',
    width: 21,
    height: 21,
},
  numcon: {
    width: 21,
    height: 21,
    marginLeft: 20,
    position:'absolute',
    alignItems: 'center',
    justifyContent: 'center',
    //borderRadius: 45,
    //backgroundColor: colors.primary,

}
});

class CartCar extends Component {
   constructor() {
    super();
     this.state = {quantityCar: 0};
    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
     'Warning: componentWillReceiveUpdate is deprecated',
     'Warning: componentDidUpdate is deprecated',
     'Warning: componentWillUpdate is deprecated',
   ]);
}

quantity = () => {
    var total = this.props.cart.reduce(function(_this, val) {
          return (_this+val.quantity)
      }, 0);
    return total;
}

  render() {
    return (

      <View style={styles.cart} >
       
       <TouchableOpacity activeOpacity={0.8} onPress={this.props.getScreen} >
         <View style={styles.imgcon} >
      <Image style={styles.carimg} source={require('../images/cart.png')}/>
        </View>

        <View style={styles.numcon}>
               {this.quantity() > 9 ? <Text style={styles.cartnum}>9+</Text>
               : this.quantity() == 0 ? null
               : this.quantity() <= 9 ? <Text style={styles.cartnum}>{this.quantity()}</Text>
               : null
               }
          </View>
        </TouchableOpacity>
        </View>
        
       )}
}

CartCar.propTypes = {
  quantity: PropTypes.number,
  getScreen: PropTypes.func
};


function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

function mapStateToProps(state, ownProps) {
  return {
    cart: state.tstore.cart
  }
}
export default connect(mapStateToProps, mapDispatchToProps) (CartCar);