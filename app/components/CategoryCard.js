import React from 'react';
import {colors} from '../configs/Colors.js';
import PropTypes from 'prop-types';
import {
  ImageBackground,
  TouchableOpacity,
  View, StyleSheet, Text
} from 'react-native';

const styles = StyleSheet.create({
  categoryContainer: {
    backgroundColor: colors.white,
    marginRight: 20,
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    borderRadius: 3
  },
  categoryImage: {
    width: 320,
    height: 213,
    justifyContent: 'center',
    alignItems: 'center'
  },
  categoryTitle: {
    color: colors.primary,
    fontFamily: "Dosis-Bold",
    justifyContent: 'center',
    alignItems: 'center',
    fontSize: 40
    
  }
});
//import { TMDB_IMG_URL } from '../../../constants/api';
//
     
const CategoryCard = ({ info, viewCategory }) => (
  <TouchableOpacity activeOpacity={0.8} onPress={viewCategory.bind(this, info.id)}>
    <View style={styles.categoryContainer}>
       
        <ImageBackground source={{uri: info.image}} borderRadius={15} style={styles.categoryImage}>
        <Text style={styles.categoryTitle} numberOfLines={1}>
          {info.title}
        </Text> 
        </ImageBackground>

    </View>
  </TouchableOpacity>
);

CategoryCard.propTypes = {
  info: PropTypes.object.isRequired,
  viewCategory: PropTypes.func.isRequired
};

export default CategoryCard;