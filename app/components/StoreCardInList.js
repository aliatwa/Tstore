import React from 'react';
import { Dimensions } from "react-native";
import {colors} from '../configs/Colors.js';
import PropTypes from 'prop-types';
const { width, height } = Dimensions.get('window');
import {
  Image,
  TouchableOpacity,
  View, StyleSheet, Text
} from 'react-native';

const styles = StyleSheet.create({
  storeContainer: {
    height: 280,
    width: width,
    //flex: 1,
    flexDirection: 'column',
    //marginRight: 20,
    borderRadius: 50,
    marginBottom: 15,
  },
  storeImage: {
    width: width-20,
    height: 213,
    borderTopLeftRadius: 8,
    borderTopRightRadius: 8
  },
  storeTitleContainer: {
    //flex: 1,
    width: width-20,
    paddingHorizontal: 10,
    justifyContent: 'center',
    borderBottomLeftRadius: 8,
    borderBottomRightRadius: 8,
    backgroundColor: colors.background,
  },
  storeTitle: {
    color: colors.secondry,
    fontFamily: "Dosis-Medium",
    fontSize: 25,
    textAlign: 'left'
  },
  storeCategories: {
    color: colors.StoreTitle,
    fontFamily: "Dosis-SemiBold",
    fontSize: 20,
    textAlign: 'left'
  },
  brandscont: {
    position: 'absolute',
    top: 175,
  },
  brandscontimgs: {
    flex: 1,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  imgbrand: {
    width: 30,
    height: 30,
    borderRadius: 45,
    marginHorizontal: 5
  },
  plusbrandcont: {
    backgroundColor: colors.primary,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    width: 30,
    height: 30,
    borderRadius: 45,
    marginHorizontal: 5
  },
  plusbrand: {
    fontFamily: "Dosis-Medium",
    fontSize: 20,
    color: colors.white,
  }
});
//import { TMDB_IMG_URL } from '../../../constants/api';
//
     
const StoreCardInList = ({ info, viewStore }) => (
  <TouchableOpacity activeOpacity={0.8} onPress={viewStore.bind(this, info.id)}>
    <View style={styles.storeContainer}>
    <Image source={{uri: info.image}} style={styles.storeImage} />
   
    <View style={styles.brandscont}>
    {info.brands.length > 0 && 
         <View style={styles.brandscontimgs}>
            {info.brands.slice(0, 3).map((brand, i, arr) => {
            return (
               <Image source={{uri: brand.img}} key={brand.id} style={styles.imgbrand} />
            )})}
    {info.brands.length > 3 &&
       <View style={styles.plusbrandcont}>
            <Text style={styles.plusbrand} numberOfLines={1}>+3</Text>
       </View>}      
    </View>}
    </View>
   
       <View style={styles.storeTitleContainer}>
          <Text style={styles.storeTitle} numberOfLines={1}>
              {info.title + ' · 100M'}
          </Text> 
        <View style={{flexDirection: 'row'}}>
          {info.categories.map((list, i, arr) => {
           return ( 
          <Text style={styles.storeCategories} key={i} numberOfLines={1}>
          {i == (arr.length - 1) ? list : list + ' · '}
        </Text> )})}
      </View>
      </View>
    </View>
  </TouchableOpacity>
);

StoreCardInList.propTypes = {
  info: PropTypes.object.isRequired,
  viewStore: PropTypes.func.isRequired
};

export default StoreCardInList;