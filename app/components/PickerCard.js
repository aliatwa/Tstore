import React, { Component } from 'react';
import { Text, View, YellowBox,StyleSheet,Dimensions, Image, ScrollView, TouchableOpacity } from 'react-native';
import {colors} from '../configs/Colors.js';
import PropTypes from 'prop-types';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    marginRight: 20,
    height: 200,
    width: 150,
    flexDirection:'column',
    justifyContent: 'flex-start',
    alignSelf: 'flex-start',
  },
  item: {
    borderRadius:5,
    backgroundColor: colors.optionBackground,
    height: 120,
    width: 120,
    alignSelf: 'flex-start',
    justifyContent: 'center',
  },
  image: {
    height: 100,
    width: 100,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  circle: {
    height: 75,
    width: 75,
    borderColor: colors.primary,
    borderRadius: 45,
    alignSelf: 'center',
    justifyContent: 'center',
  },
  label: {
    color: colors.ProductTitle,
    fontFamily: "Dosis-Medium",
    fontSize: 25,
    marginVertical:10
  },
  line: {
    width: 50,
    height: 5,
    backgroundColor: colors.primary,
  },
  customtext: {
    textAlign: 'center',
    fontSize: 30,
    fontFamily: "Dosis-Medium",
    color: colors.primary,
  }
});

     
const PickerCard = ({ info, selected, selectedItem, uuid }) => (
       <View style={styles.container}>
        <TouchableOpacity activeOpacity={0.8} onPress={selected.bind(this, info, uuid)}>
          <View style={styles.item}>
          { info.type == 'color' ? <View style={[styles.circle, {backgroundColor: info.value}]}></View>
          : info.type == 'size' ? <Image style={styles.image} source={require('../images/cart.png')}/>
          : <View style={styles.image}><Text style={styles.customtext}>{info.label.charAt(0)}</Text></View>
          } 
          </View>
          <Text style={styles.label}>{info.label}</Text>
          {selectedItem[info.type].value == info.value && 
           <View style={styles.line}></View>
          }
          
        </TouchableOpacity>
       </View>
);

PickerCard.propTypes = {
  info: PropTypes.object.isRequired,
  selectedItem: PropTypes.object,
  selected: PropTypes.func.isRequired,
};

export default PickerCard;