import React from 'react';
import {colors} from '../configs/Colors.js';
import PropTypes from 'prop-types';
import {
  Image,
  TouchableOpacity,
  View, StyleSheet, Text
} from 'react-native';

const styles = StyleSheet.create({
  Container: {
    height: 280,
    width: 200,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'center',
    //marginRight: 25,
    borderRadius: 8
  },
  brandImage: {
    width: 70,
    height: 70,
    borderRadius: 45,
  },
    brandimgborder: {
    marginBottom: 8,
    backgroundColor: colors.white,
    padding: 3,
    borderRadius: 45,
  },
  brandTitleContainer: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  brandTitle: {
    color: colors.white,
    fontFamily: "Dosis-Medium",
    fontSize: 30,
    textAlign: 'center'
  },
  storeCategories: {
    color: colors.white,
    fontFamily: "Dosis-SemiBold",
    fontSize: 20,
    textAlign: 'left'
  },
  viewbtn: {
    fontFamily: "Dosis-Medium",
    fontSize: 20,
    color: colors.white,
  },
  touch: {
    paddingHorizontal: 30,
    paddingVertical: 3,
    borderRadius: 40
  },
  touchcont: {
    backgroundColor: colors.white,
    padding: 3,
    borderRadius: 40
  }
});

const BrandCard = ({ info, viewBrand, style }) => (
    <View style={[styles.Container, style, {backgroundColor: info.color}]}>

        
      <View style={styles.brandTitleContainer}>
      <View style={styles.brandimgborder}>
        <Image source={{uri: info.image}} style={styles.brandImage} />
      </View>
        <Text style={styles.brandTitle} numberOfLines={1}>
          {info.title}
        </Text>  
      </View>

      <View style={{flexDirection: 'row'}}>
          {info.categories.slice(0,3).map((list, i, arr) => {
        return ( 
          <Text style={styles.storeCategories} key={i} numberOfLines={1}>
          {i == (arr.length - 1) ? list : list + ' · '}
          </Text> )})}
      </View>
      
      <View style={styles.touchcont}>
      <TouchableOpacity style={[styles.touch, {backgroundColor: info.color}]} activeOpacity={0.8} onPress={viewBrand.bind(this, info.id)}>
       <Text style={styles.viewbtn} numberOfLines={1}>
       View </Text>
      </TouchableOpacity>
     </View>

      </View>
);

BrandCard.propTypes = {
  info: PropTypes.object.isRequired,
  style: PropTypes.object,
  viewBrand: PropTypes.func.isRequired
};

export default BrandCard;