import React from 'react';
import {colors} from '../configs/Colors.js';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  ImageBackground,
  TouchableOpacity,ScrollView,
  View, StyleSheet, Text, Image
} from 'react-native';

const styles = StyleSheet.create({
  Container: {
    flexDirection: 'column',
    marginVertical: 10,
    marginHorizontal: 20,
    justifyContent:'center',
  },
  box: {
    height: 100,
    flexDirection: 'row',
    alignItems:'center', 
    justifyContent:'flex-start',
    marginBottom: 5
  },
  infobox: {
    marginLeft: 30,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'flex-start'
  },
  perinfo: {
    color: colors.ProductTitleDetails,
    fontSize: 22,
    fontFamily: "Dosis-SemiBold"
  },
  perinfosub: {
    color: colors.Category,
    fontSize: 17,
    fontFamily: "Dosis-SemiBold"
  },
  line: {
    backgroundColor: colors.line,
    width: '100%'-50,
    marginHorizontal: 20,
    height: 1,
  },
  go: {
    flex:1,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  }
});

const Profiletap = ({ profile, sendFeedback, setPersonalInfo, setLocation, changeAccount }) => (

    <View style={styles.Container} >
    
    <TouchableOpacity activeOpacity={0.9} style={styles.boxbtn} onPress={setPersonalInfo.bind(this)}>
    <View style={styles.box}>
    <Icon name="person" size={40} color={colors.Category} />
    <View style={styles.infobox}>
    <Text style={styles.perinfo}>Personal Information</Text>
    <Text style={styles.perinfosub}>Name, Address, Email, Phone number</Text>
    </View>
    <View style={styles.go}>
    <Icon name="keyboard-arrow-right" size={40} color={colors.ProductTitleDetails} />
    </View>
    </View>
    </TouchableOpacity>
    <View style={styles.line}/>

    <TouchableOpacity activeOpacity={0.9} style={styles.boxbtn} onPress={setLocation.bind(this)}>
    <View style={styles.box}>
    <Icon name="place" size={40} color={colors.Category} />
    <View style={styles.infobox}>
    <Text style={styles.perinfo}>Set your Location</Text>
    <Text style={styles.perinfosub}>Set your home location with GPS</Text>
    </View>
    <View style={styles.go}>
    <Icon name="keyboard-arrow-right" size={40} color={colors.ProductTitleDetails} />
    </View>
    </View>
    </TouchableOpacity>
    <View style={styles.line}/>
    
    <TouchableOpacity activeOpacity={0.9} style={styles.boxbtn} onPress={changeAccount.bind(this)}>
    <View style={styles.box}>
    <Icon name="store" size={40} color={colors.Category} />
    <View style={styles.infobox}>
    <Text style={styles.perinfo}>Become a Seller/Brand</Text>
    <Text style={styles.perinfosub}>Change your Account</Text>
    </View>
    <View style={styles.go}>
    <Icon name="keyboard-arrow-right" size={40} color={colors.ProductTitleDetails} />
    </View>
    </View>
    </TouchableOpacity>
    <View style={styles.line}/>

    <TouchableOpacity activeOpacity={0.9} style={styles.boxbtn} onPress={sendFeedback.bind(this)}>
    <View style={styles.box}>
    <Icon name="feedback" size={40} color={colors.Category} />
    <View style={styles.infobox}>
    <Text style={styles.perinfo}>Feedback</Text>
    <Text style={styles.perinfosub}>Send Feedback</Text>
    </View>
    <View style={styles.go}>
    <Icon name="keyboard-arrow-right" size={40} color={colors.ProductTitleDetails} />
    </View>
    </View>
    </TouchableOpacity>

    </View>
);

Profiletap.propTypes = {
  profile: PropTypes.object.isRequired,
  sendFeedback: PropTypes.func.isRequired,
  setPersonalInfo: PropTypes.func.isRequired,
  setLocation: PropTypes.func.isRequired,
  changeAccount: PropTypes.func.isRequired,
};

export default Profiletap;