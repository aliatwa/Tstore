import React, { Component } from 'react';
import { Text, View, YellowBox,StyleSheet,Dimensions, Image, ScrollView, TouchableOpacity } from 'react-native';
import {colors} from '../configs/Colors.js';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  item: {
    marginHorizontal: 20,
    height: 150,
    flexDirection:'row',
    justifyContent: 'flex-start',
  },
  placeorder: {
    color: colors.white,
    fontSize: 23,
    fontFamily: "Dosis-SemiBold",
  },
  order: {
    backgroundColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: (width*1/6),
    marginVertical: 50,
    borderRadius: 45,
    width: width*(2/3),
    paddingVertical: 20
  },
  removeitem: { 
    alignItems:'flex-start', 
    justifyContent:'center',
    marginHorizontal:10
  },
  imagev: {
    alignItems:'center', 
    justifyContent:'center',
    marginLeft: 25,
  },
  image: {
    borderRadius: 5,
    width: 125,
    height: 125,
  },
  product: {
    marginLeft: 25,
    marginVertical: 10,
    flex: 1,
    justifyContent: 'space-around',
    flexDirection: 'column',
    alignItems: 'flex-start', 
  },
  name: {
    color: colors.ProductTitle,
    fontFamily: "Dosis-Medium",
    fontSize: 24,
    textAlign: 'left'
  },
  price: {
    color: colors.secondry,
    fontFamily: "Dosis-SemiBold",
    fontSize: 20,
    textAlign: 'left'
  },
  quantity: {
    flexDirection: 'column',
    justifyContent: 'flex-end',
    alignSelf: 'center',
    alignItems: 'center',
    marginHorizontal:10
  },
  qnum: {
    color: colors.primary,
    fontSize: 25,
    fontFamily: "Dosis-SemiBold",
  },
  plustext: {
    color: colors.secondry,
    fontSize: 30,
    fontFamily: "Dosis-Medium",
  },
  circle: {
    height: 20,
    width: 20,
    borderRadius: 45,
    alignSelf: 'center',
    justifyContent: 'center',
    textAlign: 'center',
    marginRight: 10
  },
  textoption: {
    height: 20,
    width: 20,
    color: colors.primary,
    backgroundColor: colors.optionBackground,
    fontFamily: "Dosis-SemiBold",
    borderRadius: 45,
    alignSelf: 'center',
    textAlign: 'center',
    justifyContent: 'center',
    marginRight: 10
  },
  optionsContainer: {
    flexDirection: 'row',
    alignSelf: 'flex-start',
    justifyContent: 'flex-start',
  }
});

     
const CartCard = ({ info, removeItem, minus, plus }) => (
       <View style={styles.item}>
        <TouchableOpacity style={styles.removeitem} onPress={removeItem.bind(this, info)}>
        <Icon name="remove-circle-outline" size={30} color={colors.remove_circle} />
        </TouchableOpacity>
          <View style={styles.imagev}>
          <Image style={styles.image} source={{uri: info.image}}/>
          </View>


         <View style={styles.product}>
           <Text style={styles.name}>{info.title}</Text>
           <Text style={styles.price}>{info.price} L.E</Text> 

        {info.selectedOptions.length > 0 && 
       <View style={styles.optionsContainer}>
         {info.selectedOptions.map((options, i, arr) => { 
        if (options.color) {
        return(<Text key={i} style={[styles.circle, {backgroundColor: options.color.value, color: options.color.value}]}>.</Text>)}
         else if (options.size) {
         return( <Text key={i} style={styles.textoption}>{options.size.label.charAt(0)}</Text>)
         } else {
          return(null)
         }
          })}
          </View>
        }


          </View>
         <View style={styles.quantity}>
                  <TouchableOpacity style={styles.plus} onPress={plus.bind(this, info.tmpId)}>
                     <Text style={styles.plustext}>+</Text>
                  </TouchableOpacity>
                  <Text style={styles.qnum}>{info.quantity}</Text>
                  <TouchableOpacity style={styles.mins} onPress={minus.bind(this, info.tmpId)}>
                     <Text style={styles.plustext}>-</Text>
                  </TouchableOpacity>
         </View>

       </View>
);

CartCard.propTypes = {
  info: PropTypes.object.isRequired,
  removeItem: PropTypes.func.isRequired,
  minus: PropTypes.func.isRequired,
  plus: PropTypes.func.isRequired
};

export default CartCard;