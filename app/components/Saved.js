import React from 'react';
import {colors} from '../configs/Colors.js';
import PropTypes from 'prop-types';
import { strings } from '../locales/i18n';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {
  ImageBackground,
  TouchableOpacity,ScrollView,
  View, StyleSheet, Text, Image
} from 'react-native';

const styles = StyleSheet.create({
  box: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    marginVertical: 25,
    marginHorizontal: 20,
  },
  Container: {
    width: '50%',
    height: 151,
    flexDirection: 'column',
    alignItems:'flex-start', 
    justifyContent:'center',
    marginBottom: 5
  },
  removesaved: { 
    width: '50%',
    flexDirection: 'row',
    position: 'absolute',
    alignItems:'flex-end', 
    right: 5,
    top: 0
  },
  imgcont: {
    alignItems:'flex-start', 
    justifyContent:'flex-start',
  },
  productimg: {
    borderRadius: 5,
    width: 151,
    height: 151,
  },
  cardPrice: {
    color: colors.white,
    backgroundColor: colors.primary,
    borderRadius: 4,
    paddingVertical: 3,
    paddingHorizontal: 5,
    fontFamily: "Dosis-SemiBold",
    fontSize: 15,
    textAlign: 'center'
  },
  hovercont: {
    top: 116,
    paddingHorizontal: 7,
    flexDirection: 'row',
    position: 'absolute',
    width: '100 %',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  brandimg: {
    width: 30,
    height: 30,
    borderRadius: 45
  },
  cardTitleContainer: {
    flexDirection: 'column',
    justifyContent: 'center'
  },
  cardTitle: {
    color: colors.ProductTitleDetails,
    fontFamily: "Dosis-Medium",
    fontSize: 23,
    textAlign: 'left'
  },
});

const Saved = ({ saved, viewProduct, removeSaved }) => (
  <View style={styles.box}>
  {saved.map((info) => {
    return(
    <View style={styles.Container} key={info.id}>

  <TouchableOpacity activeOpacity={0.8} onPress={viewProduct.bind(this, info.id)}>
      <View style={styles.imgcont}>
            <Image style={styles.productimg} source={{uri: info.image}}/>
            <View style={styles.hovercont}>
              <Image source={{ uri: info.brandimg }} style={styles.brandimg} />
              <Text style={styles.cardPrice} numberOfLines={1}>
                {strings('confirm.priceconfirm', { price: info.price })}
              </Text>
            </View>
      </View>
  </TouchableOpacity>
        <TouchableOpacity style={styles.removesaved} onPress={removeSaved.bind(this, info.id)}>
          <Icon name="favorite" size={30} color={colors.favorite} />
        </TouchableOpacity>

        <View style={styles.cardTitleContainer}>
          <Text style={styles.cardTitle} numberOfLines={1}>
            {info.title}
          </Text>
        </View>
  
    </View>
    )})}
    </View>
);

Saved.propTypes = {
  saved: PropTypes.array.isRequired,
  viewProduct: PropTypes.func.isRequired,
  removeSaved: PropTypes.func.isRequired,
};

export default Saved;