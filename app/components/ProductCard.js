import React from 'react';
import {colors} from '../configs/Colors.js';
import { strings } from '../locales/i18n';
import PropTypes from 'prop-types';
import {
  Image,
  TouchableOpacity,
  View, StyleSheet, Text
} from 'react-native';

const styles = StyleSheet.create({
  cardContainer: {
    height: 200,
    width: 151,
    backgroundColor: colors.white,
    flexDirection: 'column',
    marginRight: 20,
    justifyContent: 'flex-start'
  },
  cardImage: {
    width: 151,
    height: 151,
    borderRadius: 5,
  },
  cardTitleContainer: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center'
  },
  cardTitle: {
    color: colors.ProductTitleDetails,
    fontFamily: "Dosis-Medium",
    fontSize: 23,
    textAlign: 'left',
    //marginTop: 5
  },
  cardPrice: {
    color: colors.white,
    backgroundColor: colors.primary,
    borderRadius: 4,
    paddingVertical: 3,
    paddingHorizontal: 5,
    fontFamily: "Dosis-SemiBold",
    fontSize: 15,
    textAlign: 'center'
  },
  hovercont: {
    bottom: 54,
    paddingHorizontal: 7,
    flexDirection: 'row',
    position: 'absolute',
    width: '100 %',
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  brandimg: {
    width: 30,
    height: 30,
    borderRadius: 45
  }
});
//import { TMDB_IMG_URL } from '../../../constants/api';
//
     
const ProductCard = ({ info, viewProduct }) => (
  <TouchableOpacity activeOpacity={0.8} onPress={viewProduct.bind(this, info.id, info)}>
    <View style={styles.cardContainer}>
    
    <Image source={{uri: info.image}} style={styles.cardImage} />

    <View style={styles.hovercont}>
        <Image source={{ uri: info.brandimg }} style={styles.brandimg} />
        <Text style={styles.cardPrice} numberOfLines={1}>
            {strings('confirm.priceconfirm', {price: info.price})}
        </Text>
    </View>

    <View style={styles.cardTitleContainer}>
        <Text style={styles.cardTitle} numberOfLines={1}>
          {info.title}
        </Text>   
    </View>

    </View>
  </TouchableOpacity>
);

ProductCard.propTypes = {
  info: PropTypes.object.isRequired,
  viewProduct: PropTypes.func.isRequired
};

export default ProductCard;