import React, { Component } from 'react';
import { Text, View, Animated, YellowBox,StyleSheet,Dimensions, Image, ScrollView, TouchableOpacity } from 'react-native';
import {colors} from '../configs/Colors.js';
import Icon from 'react-native-vector-icons/MaterialIcons';
import PropTypes from 'prop-types';

const styles = StyleSheet.create({
  animatedView: {
    backgroundColor: colors.background,
    position: 'absolute',
    left: 0,
    bottom: 0,
    right: 0,
    height: 60,
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  snackBarText: {
    color: colors.primary,
    fontSize: 20,
    fontFamily: "Dosis-Meduim"
  },
  icon: {
   // position: 'absolute',
    marginRight: 10,
    justifyContent: 'center',
    padding: 5
  },

});

class snackBar extends Component {
   constructor() {
    super();
    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
     'Warning: componentWillReceiveUpdate is deprecated',
     'Warning: componentDidUpdate is deprecated',
     'Warning: componentWillUpdate is deprecated',
   ]);
   this.animatedValue = new Animated.Value(60);
   this.snackBarShown = false;
   this.snackBarHidden = true;
   this.state = { message: '', icon: 'none' };
}

componentWillUnmount() {
    clearTimeout(this.timerID);
}

show(message="Default Message...", duration=3000, icon="none") {
   // Code responsible to show SnackBar.
   if( this.snackBarShown === false )
   {
     this.setState({ message: message, icon: icon });
     this.snackBarShown = true;
     Animated.timing
     (
         this.animatedValue,
         { 
             toValue: 0,
             duration: 350
         }
     ).start(this.hide(duration));
   }      
}

hide = (duration) => {
   // Code responsible to hide SnackBar automatically after a given time duration.
   this.timerID = setTimeout(() =>
   {
     if(this.snackBarHidden === true)
     {
         this.snackBarHidden = false;

         Animated.timing
         (
           this.animatedValue,
           { 
             toValue: 60,
             duration: 350
           }
         ).start(() =>
         {
           this.snackBarHidden = true;
           this.snackBarShown = false;
           clearTimeout(this.timerID);
         })
     }
   }, duration); 
}

closeSnackBar = () => {
   // Code responsible to hide SnackBar on Button click
   if(this.snackBarHidden === true)
   {
       this.snackBarHidden = false;
       clearTimeout(this.timerID);   
       Animated.timing
       (
           this.animatedValue,
           { 
             toValue: 60,
             duration: 350
           }
       ).start(() =>
       {
           this.snackBarShown = false;
           this.snackBarHidden = true;
       });
   }
}

  render() {
    return (
      <Animated.View style = {[{ transform: [{ translateY: this.animatedValue }]}, styles.animatedView ]}>
        <Text numberOfLines = { 1 } style = {styles.snackBarText}>{ this.state.message }</Text>
        <View style = { styles.icon }>
             { this.state.icon == 'done' ? <Icon name="check-circle" size={25} color={colors.green} /> 
             : this.state.icon == 'warn' ? <Icon name="warning" size={25} color={colors.warning} />
             : this.state.icon == 'info' ? <Icon name="error" size={25} color={colors.activesentiment} />
             : null
             }
        </View>
      </Animated.View>
       )}
}

export default snackBar;