import React, { Component } from 'react';
import { Text, View, YellowBox, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import { colors } from '../configs/Colors.js';
import { strings } from '../locales/i18n';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
    container: {
        padding: 17,
        height: height * (1 / 2),
        width: width * (2 / 3),
        flexDirection: 'column',
        backgroundColor: colors.white,
        justifyContent: 'space-between',
        borderRadius: 7,
    },
    header: {
        marginBottom: 10,
        flexDirection: 'row',
        alignItems: 'center',
        justifyContent: 'flex-end'
    },
    modaltitle: {
        color: colors.ProductTitleDetails,
        fontSize: 18,
        fontFamily: "Dosis-SemiBold",
    },
    productimgcont: {
        flex: 1,
        marginBottom: 8
    },
    brandimg: {
        borderRadius: 5,
        width: '100%',
        height: '100%',
    },
    productinfo: {
        flexDirection: 'column',
        alignItems: 'flex-start',
        justifyContent: 'center'
    },
    name: {
        color: colors.ProductTitleDetails,
        fontSize: 25,
        fontFamily: "Dosis-Medium",
    },
    price: {
        color: colors.ProductTitleDetails,
        fontSize: 20,
        fontFamily: "Dosis-Medium",
    },
});

class ProductMini extends Component {
    constructor(props) {
        super(props);
    //    this.state = {  };
        YellowBox.ignoreWarnings([
            'Warning: componentWillMount is deprecated',
            'Warning: componentWillReceiveProps is deprecated',
        ]);
        this.close = this.close.bind(this);
    }

close() {
        console.log('close view');
        this.props.navigator.dismissLightBox();
    }

componentDidMount() {

    }

    render() {
        return (
            <View style={styles.container}>

                <View style={styles.header}>
                    <TouchableOpacity activeOpacity={0.8} onPress={this.close} >
                        <Icon name="close" size={25} color={colors.ProductTitleDetails} />
                    </TouchableOpacity>
                </View>

                <View style={styles.productimgcont}>
                    <Image style={styles.brandimg} source={{ uri: this.props.viewData.image }} />
                </View>

                <View style={styles.productinfo}>
                    <Text style={styles.name}>{this.props.viewData.title}</Text>
                    <Text style={styles.price}>{strings('confirm.priceconfirm', { price: this.props.viewData.price })}</Text>
                </View>

            </View>
        );
    }
}

export default ProductMini;
