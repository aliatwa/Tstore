import { createStore, applyMiddleware, compose } from 'redux';
import thunk from 'redux-thunk';
import {persistCombineReducers} from 'redux-persist';
import logger from 'redux-logger';
import storage from 'redux-persist/es/storage';
import rootReducer from '../reducres/rootReducer';

let middleware = [thunk];


const persistConfig = {
  key: 'root',
  storage,
}
const reducer = persistCombineReducers(persistConfig, rootReducer)
if (__DEV__) {
	const reduxImmutableStateInvariant = require('redux-immutable-state-invariant').default();
	middleware = [...middleware, reduxImmutableStateInvariant, logger];
} else {
	middleware = [...middleware];
}

export default function configureStore (initialState) {
  return createStore(
    reducer,
    initialState,
   applyMiddleware(...middleware)
  )
}