import React, { Component } from 'react';
import { Text, View,Linking, YellowBox, BackHandler, StyleSheet,Dimensions, Image, ScrollView, TouchableOpacity } from 'react-native';
import { strings } from '../locales/i18n';
import {colors} from '../configs/Colors.js';
import { bindActionCreators } from 'redux';
import * as Actions from '../reducres/tstore.actions';
import { connect } from 'react-redux';
import MapView, { Marker } from 'react-native-maps';
import Polyline from '@mapbox/polyline';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');

const navigatorStyle = {
  statusBarColor: colors.white,
  statusBarTextColorScheme: 'dark',
  navigationBarColor: colors.white,
  navBarBackgroundColor: colors.white,
  screenBackgroundColor: colors.white,
  tabBarSelectedButtonColor: 'red',
  tabBarBackgroundColor: 'white',
  topBarElevationShadowEnabled: false,
  navBarHideOnScroll: false,
  tabBarHidden: true,
  drawUnderTabBar: true,
  navBarHidden: false,
  navBarTitleTextCentered: true
};


const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'flex-start',
    backgroundColor: colors.background
  },
  containermap: {
    top: 0,
    left: 0,
    right: 0,
    bottom: 0,
    width: width,
    height: height*(1/3),
    justifyContent: "flex-end",
    alignItems: "center"
  },
  map: {
    position: "absolute",
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  },
  textgetdirections: {
    color: colors.primary,
    fontSize: 20,
    fontFamily: "Dosis-SemiBold",
  },
  getdirections: {
    flex:1,
    alignItems: 'center',
    justifyContent: 'center',
    borderRadius: 45,
    width: 200,
    paddingVertical: 15,
    paddingHorizontal: 5
  },
  storecontainer: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    marginTop: 10,
    height: 130,
    backgroundColor: colors.white
  },
  storeimg: {
    width: 100,
    height: 100,
    borderRadius: 3
  },
  storeimgcon: {
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  storeinfo: {
    flexDirection: 'column',
    marginLeft: 10,
    marginVertical: 8,
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    backgroundColor: colors.white
  },
  name: {
    color: colors.ProductTitleDetails,
    fontSize: 30,
    fontFamily: 'Dosis-Medium'
  },
  time: {
    color: colors.Category,
    fontSize: 20,
    fontFamily: 'Dosis-SemiBold'
  },
  distance: {
    color: colors.Category,
    fontSize: 20,
    fontFamily: 'Dosis-SemiBold'
  },
  address: {
    color: colors.Category,
    fontSize: 17,
    fontFamily: "Dosis-Regular"
  },
  addresscon: {
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    marginLeft: 15,
  },
  confirmcon: {
    flex:1,
    justifyContent: 'center',
    //alignItems: 'center',
    //alignSelf: 'center',
    marginVertical:20,
    marginHorizontal: 20,
    paddingHorizontal: 30,
    backgroundColor: colors.white
  },
  total: {
    color: colors.allDone,
    fontSize: 17,
    fontFamily: 'Dosis-Bold'
  },
  pricecon: {
    justifyContent: 'center',
    alignItems: 'center',
  },

  textconfirm: {
    color: colors.white,
    fontSize: 30,
    fontFamily: 'Dosis-Medium'
  },
  confirm: {
    justifyContent: 'center',
    alignItems: 'center',
    backgroundColor: colors.primary,
    borderRadius: 45,
    width: width*(1/2),
    paddingVertical: 10,
    paddingHorizontal: 10
  },
  productcost: {
    //flex: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  price: {
    color: colors.primary,
    fontSize: 25,
    fontFamily: 'Dosis-Light'
  },
  producttext: {
    color: colors.primary,
    fontSize: 30,
    fontFamily: 'Dosis-Light'
  },
  line: {
    backgroundColor: colors.line,
    height: 1,
    marginVertical: 20
  },
  totalcost: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between',
  },
  pricetotal: {
    color: colors.primary,
    fontSize: 25,
    fontFamily: 'Dosis-Medium'
  },
  totaltext: {
    color: colors.primary,
    fontSize: 30,
    fontFamily: 'Dosis-Medium'
  },
  payment: {
    flexDirection: 'row',
    justifyContent: 'center',
    marginVertical: 20
  }

});


class Confirm extends Component {
   constructor(props) {
    super(props);
  this.state = {distance: null, coords:[], address: null, servicefee: null, productcost: null,total: null, mapImage:null};

    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
     'Warning: componentWillReceiveUpdate is deprecated',
     'Warning: componentDidUpdate is deprecated',
     'Warning: componentWillUpdate is deprecated',
   ]);

      this.openGoogleMaps = this.openGoogleMaps.bind(this);
      this.allDone = this.allDone.bind(this);
      this.orderValue = this.orderValue.bind(this);
}

async getDirections(startLoc, destinationLoc) {

         var that = this;
         fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${ startLoc }&destination=${ destinationLoc }`).
         then(function(responseText){ 
                var response = responseText.json();
                response.then(function(response){
                if (response != undefined) {
                that.setState({distance: response.routes[0].legs[0].distance.text});}
                });
         });
         fetch(`https://maps.googleapis.com/maps/api/geocode/json?latlng=${ destinationLoc }&language=en`).
         then(function(responseText){ 
                var response = responseText.json();
                response.then(function(response){
                //var address = response.results[0].formatted_address;
                var address = 'Mohammed Ahmed Ibrahim, Al Manteqah Ath Thamenah, Nasr City, Cairo Governorate';
                that.setState({address: address});
                });
         });
            var markers = 'markers=color:0x7d51ff' + encodeURI('|') + 'label:T' + encodeURI('|' + destinationLoc);
            var mapImage = `https://maps.googleapis.com/maps/api/staticmap?size=500x500&maptype=roadmap&${ markers }`;
            this.setState({mapImage: mapImage})

            try {
                    let resp = `https://maps.googleapis.com/maps/api/directions/json?origin=${ startLoc }&destination=${ destinationLoc }`;
                    let addressresp = `https://maps.googleapis.com/maps/api/geocode/json?latlng=${ destinationLoc }&language=en`;
                    var urls = [resp, addressresp];
                    let respJson ;//= await resp.json();
                    //let respaddressresp = await addressresp.json();
                   // var address ;//= respaddressresp.results.formatted_address;
                    //let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
                    //let distance = respJson.routes[0].legs[0].distance.text;
                    //var polyline = encodeURI(respJson.routes[0].overview_polyline.points);
                    //var start = 'markers=color:green' + encodeURI('|' + startLoc);
                    
                    //var markers = 'markers=icon:https://storage.googleapis.com/material-icons/external-assets/v4/icons/svg/ic_place_black_24px.svg' + encodeURI('|' + destinationLoc);
                    //var markers = [start, end];
                    //markers = markers.join('&');        

                    //console.log(respaddressresp)
                    /*let coords = points.map((point, index) => {
                        return  {
                            latitude : point[0],
                            longitude : point[1]
                        }
                    })*/
                    //var mapImage = `https://maps.googleapis.com/maps/api/staticmap?size=500x500&maptype=roadmap&path=enc:${ polyline }&${ markers }`;
                    //this.setState({ distance: 0, mapImage: mapImage})
                    return ;
            } catch(error) {
              console.log(error)
            alert(error)
            return error
        }
        /*try {
            let resp = await fetch(`https://maps.googleapis.com/maps/api/directions/json?origin=${ startLoc }&destination=${ destinationLoc }`)
            let respJson = await resp.json();
            let points = Polyline.decode(respJson.routes[0].overview_polyline.points);
            let distance = respJson.routes[0].legs[0].distance.text;
            var polyline = encodeURI(respJson.routes[0].overview_polyline.points);
            console.log(respJson)
            let coords = points.map((point, index) => {
                return  {
                    latitude : point[0],
                    longitude : point[1]
                }
            })
            this.setState({coords: coords, distance:distance})
            return coords
            } catch(error) {
            alert(error)
            return error
            }*/
    }


openGoogleMaps(url) {
  /*Linking.canOpenURL(url).then(supported => {
  if (!supported) {
    console.log('Can\'t handle url: ' + url);
  } else {
    return Linking.openURL(url);
  }
 }).catch(err => console.error('An error occurred', err));*/
}
allDone() {
  const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"];
  const date = monthNames[new Date().getMonth()]+' '+ new Date().getDate()+', '+new Date().getFullYear();
  var order = {};
  order.id = Math.floor(Math.random() * 10000);
  order.status = 'Pending';
  order.storeid = 1425;
  order.totalprice = this.state.total;
  order.totalfee = this.state.servicefee;
  order.productcost = this.state.productcost;
  order.orderdate = date,
  //order.orderdate = new Date(date),
  order.backet = this.props.cart;
  console.log(order);
  this.props.actions.placeOrder(order); 

  this.props.navigator.showModal({
  screen: "AllDone", 
  navigatorStyle,
   leftButtons: [{
        id: 'goHome',
        icon: require('../images/left.png')
      }],
  overrideBackPress: false,
  });
}

orderValue() {

}

componentDidMount () {
    
  var productcost = this.props.cart.reduce(function(_this, val) {
          return (_this+(val.quantity*val.price))
      }, 0);
  var markup = 10;
  var shippingFee = 5;
  var selling = ((productcost) / (100 - markup))* 100;
  var total = (shippingFee + Math.ceil(selling/5)*5).toFixed(2);
  var servicefee = ((total-productcost)).toFixed(2)

  this.setState({ productcost: productcost.toFixed(2), total: total, servicefee: servicefee});
  //BackAndroid.addEventListener('hardwareBackPress', () => {return true});
  //BackAndroid.removeEventListener('hardwareBackPress', () => {return true});
 this.getDirections("30.509936, 31.151507", "30.465988, 31.184692")
}

render() {
    return (
      <View style={styles.container}>

       <View style={styles.containermap}>
      {/*<MapView style={styles.map} initialRegion={{
                latitude:30.510402, 
                longitude:31.153082, 
                latitudeDelta: 0.0922,
                longitudeDelta: 0.0421
                  }}>
                  <MapView.Polyline 
                  coordinates={this.state.coords}
                  strokeWidth={3}
                  strokeColor={colors.primary}/>
       </MapView>*/}
        <Image style={styles.containermap} source={{uri: this.state.mapImage}}/>
      </View>
        <View style={styles.storecontainer}>
                   <View style={styles.storeimgcon}>
                     <Image style={styles.storeimg} source={{uri: 'https://images.pexels.com/photos/237718/pexels-photo-237718.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'}}/>
                   </View>
          
                   <View style={styles.storeinfo}>
                    <Text style={styles.name}>Storey</Text>
                    <Text style={styles.time}>10AM · 8PM</Text> 
                    <Text style={styles.distance}>{this.state.distance}</Text> 
                   </View>
            
                   <View style={styles.addresscon}>
                    <Text style={styles.address} numberOfLines={4}>{this.state.address}</Text>
                   </View>
           {/*<TouchableOpacity activeOpacity={0.8} style={styles.getdirections}
            onPress={this.openGoogleMaps('google.navigation:q=30.065988,31.084692')}>
                       <Text style={styles.textgetdirections}>{strings('confirm.openGoogleMaps')}</Text>
                       <Icon name="explore" size={40} color={colors.primary} />
                     </TouchableOpacity>*/}
        </View>

        <View style={styles.confirmcon}>
          {/*<View style={styles.pricecon}>
                    <Text style={styles.total}>{strings('confirm.total')}</Text>
                    <Text style={styles.price}>{strings('confirm.priceconfirm', { price: this.state.productcost })}</Text>
          </View>*/}

          <View style={styles.productcost}>
           <Text style={styles.producttext}>Product Cost</Text>
           <Text style={styles.price}>{strings('confirm.priceconfirm', { price: this.state.productcost })}</Text>          
          </View>
          <View style={styles.productcost}>
           <Text style={styles.producttext}>Service Fee</Text>
           <Text style={styles.price}>{strings('confirm.priceconfirm', { price: this.state.servicefee })}</Text>          
          </View>
          <View style={styles.line}/>
          <View style={styles.totalcost}>
           <Text style={styles.totaltext}>Total</Text>
           <Text style={styles.pricetotal}>{strings('confirm.priceconfirm', { price: this.state.total })}</Text>          
          </View>
          
        <View style={styles.payment}>
          <TouchableOpacity activeOpacity={0.8} style={styles.confirm} onPress={this.allDone}>
            <Text style={styles.textconfirm}>{strings('confirm.confirm')}</Text>
          </TouchableOpacity>
          </View>
        </View>
          
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

function mapStateToProps(state, ownProps) {
  return {
    cart: state.tstore.cart
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (Confirm);
