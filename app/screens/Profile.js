import React, { Component } from 'react';
import { Text, View, YellowBox,StyleSheet,Dimensions, Image, ScrollView, TouchableOpacity } from 'react-native';
import { strings } from '../locales/i18n';
import {colors} from '../configs/Colors.js';
import CartCard from '../components/CartCard';
import Orders from '../components/Orders';
import Saved from '../components/Saved';
import Profiletap from '../components/Profile';
import { bindActionCreators } from 'redux';
import * as Actions from '../reducres/tstore.actions';
import { connect } from 'react-redux';
import SnackBar from '../components/SnackBar';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import DefaultTabBar from '../components/DefaultTabBar';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');

const navigatorStyle = {
  statusBarColor: colors.white,
  statusBarTextColorScheme: 'dark',
  navigationBarColor: colors.white,
  navBarBackgroundColor: colors.white,
  screenBackgroundColor: colors.white,
  tabBarSelectedButtonColor: 'red',
  tabBarBackgroundColor: 'white',
  topBarElevationShadowEnabled: false,
  navBarHideOnScroll: false,
  tabBarHidden: true,
  drawUnderTabBar: true,
 // navBarHidden: true,
  navBarTitleTextCentered: true
};


const styles = StyleSheet.create({
  container:{
    flex: 1,
    //marginVertical: 40,
    justifyContent: 'center',
    flexDirection: 'column'
  },
  usercontainer: {
    //flex: 1,
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
    marginVertical: 5,
    marginHorizontal: 40,
    height: 110
  },
  userimg: {
  	height: 100,
  	width: 100,
  	borderRadius: 6,
  	marginBottom: 15
  },
  username: {
    fontFamily: "Dosis-Regular",
  	color: colors.ProductTitleDetails,
    fontSize: 30,
    marginLeft: 20
  },
  contentContainer: {
    flex: 1
  },
  textStyle: {
		color: colors.secondry,
		paddingTop: 10,
    fontSize: 16,
    fontFamily: "Dosis-SemiBold"
  },
  underlineStyle: {
    backgroundColor: colors.primary
  },
  tabBar: {
    backgroundColor: colors.white,
    marginHorizontal: 40
	},
});


function getDetails() {
     var json =  {
     "profile": { "id": 134,
       "name": "Eliza Stevensons",
       "email": "ali.atwa2040@gmail.com",
       "phone": 1099388351,
       "address": "fake address",
       "government": "mnf",
       "interests": ['shoes','bags','clothes'],
       "gender": "male",
       "location": [30.569889,31.55784884],
       "image": "https://images.unsplash.com/photo-1502326248588-7e9fa7032efa?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f79cd2ef5b77b97337005fb485c67e61&auto=format&fit=crop&w=666&q=80",
     }             
                    };
      return json;
}

class Profile extends Component {
   constructor(props) {
    super(props);
    this.state = {
      total: 0,
      tab: 0,
    };
    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
     'Warning: componentWillReceiveUpdate is deprecated',
     'Warning: componentDidUpdate is deprecated',
     'Warning: componentWillUpdate is deprecated',
   ]);
   
   this._getOrder = this._getOrder.bind(this);
   this._getProduct = this._getProduct.bind(this);
   this._removeOrder = this._removeOrder.bind(this);
   this._removeSaved = this._removeSaved.bind(this);
   this._sendFeedback = this._sendFeedback.bind(this);
   this._changeAccount = this._changeAccount.bind(this);
   this._setLocation = this._setLocation.bind(this);
   this._setPersonalInfo = this._setPersonalInfo.bind(this);
}

componentWillMount () {
  if  (this.props.tab) {
    this.setState({ tab: this.props.tab });
  }
}


_getOrder(orderid) {
 console.log('_getOrder', orderid);
 var order = this.props.orders.filter(function(a) { return a['id']===orderid; })[0]
 this.props.navigator.showLightBox({
  screen: "OrderInfo", 
  passProps: {
    order
  },
  style: {
    backgroundBlur: "dark", 
    backgroundColor: "#00000080", 
    tapBackgroundToDismiss: true 
  },
  adjustSoftInput: "resize", // android only, adjust soft input, modes: 'nothing', 'pan', 'resize', 'unspecified' (optional, default 'unspecified')
 });
}
_getProduct(productid) {
 console.log('_getProduct', productid);
}
_sendFeedback() {
 console.log('_sendFeedback');
 this.props.navigator.showLightBox({
  screen: "Feedback", 
  passProps: {
    onUnmout: () => { this.refs.SnackBar.show("Feedback Sent Successfully", 1500, "done"); }
  },
  style: {
    backgroundBlur: "dark", 
    backgroundColor: "#00000080", 
    tapBackgroundToDismiss: true 
  },
  adjustSoftInput: "resize", // android only, adjust soft input, modes: 'nothing', 'pan', 'resize', 'unspecified' (optional, default 'unspecified')
 });
}
_changeAccount() {
 console.log('_changeAccount');
 this.props.navigator.showLightBox({
  screen: "ChangeAccount", 
  passProps: {
    onUnmout: () => { this.refs.SnackBar.show("Account Updated Successfully", 1500, "done"); }
  },
  style: {
    backgroundBlur: "dark", 
    backgroundColor: "#00000080", 
    tapBackgroundToDismiss: true 
  },
  adjustSoftInput: "resize", // android only, adjust soft input, modes: 'nothing', 'pan', 'resize', 'unspecified' (optional, default 'unspecified')
 });
}
_setLocation() {
  var that = this;
 console.log('_setLocation');
 navigator.geolocation.getCurrentPosition(
  (position) => {
    console.log(position.coords.latitude, ' $ ', position.coords.longitude)
    that.props.actions.setLocation(position.coords.latitude, position.coords.longitude);
    that.refs.SnackBar.show("Location Saved Successfully", 1500, "done");
  },
  (error) => {
    if (error.code == 2) {
      this.refs.SnackBar.show("Turn on GPS first ", 1500, "warn");
    console.log(error.code,error.message)
    } else if (error.code == 2) {
      this.refs.SnackBar.show("Permission Denied", 1500, "warn");
    }
    },
  { enableHighAccuracy: true, timeout: 20000, maximumAge: 1000 },
);
}
_setPersonalInfo() {
 console.log('_setPersonalInfo');
 this.props.navigator.showLightBox({
  screen: "PersonalInfo", 
  passProps: {
    data: this.props.profile,
    onUnmout: (data) => {
      this.props.actions.updateProfile(data);
      this.props.navigator.dismissLightBox();
      this.refs.SnackBar.show("Profile Updated Successfully", 1500, "done"); },
  },
  style: {
    backgroundBlur: "dark", 
    backgroundColor: "#00000080", 
    tapBackgroundToDismiss: true 
  },
  adjustSoftInput: "resize", // android only, adjust soft input, modes: 'nothing', 'pan', 'resize', 'unspecified' (optional, default 'unspecified')
 });
}
_removeOrder(orderid) {
 console.log('_removeOrder', orderid);
 this.props.actions.removeOrder(orderid); 
}
_removeSaved(data) {
 console.log('_removeItem', data);
 this.props.actions.removeSaved(data); 
}

render() {
  const info = getDetails();
    return (
      <View style={styles.container}>
             <View style={styles.usercontainer}>
               <Image style={styles.userimg} source={{uri: info.profile.image}}/>
               <Text style={styles.username}>{info.profile.name}</Text>
             </View>
        <View style={styles.contentContainer}>
						<ScrollableTabView initialPage={this.state.tab}
							renderTabBar={() => (
								<DefaultTabBar
									textStyle={styles.textStyle}
									underlineStyle={styles.underlineStyle}
									style={styles.tabBar}
								/>
							)}>
              <ScrollView vertical showsVerticalScrollIndicator={false} tabLabel="ORDERS">
							<Orders viewOrder={this._getOrder} removeOrder={this._removeOrder}  orders={this.props.orders} />
              </ScrollView>

              <ScrollView vertical showsVerticalScrollIndicator={false} tabLabel="SAVED">
							<Saved viewProduct={this._getProduct} removeSaved={this._removeSaved} saved={this.props.saved} />
              </ScrollView>

              <ScrollView vertical showsVerticalScrollIndicator={false} tabLabel="PROFILE">
              <Profiletap setLocation={this._setLocation}
                          setPersonalInfo={this._setPersonalInfo}
                          changeAccount={this._changeAccount}
                          sendFeedback={this._sendFeedback} 
                          profile={this.props.profile} />
              </ScrollView>
              
						</ScrollableTabView>
				</View>
        <SnackBar ref="SnackBar" />
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

function mapStateToProps(state, ownProps) {
  return {
    cart: state.tstore.cart,
    orders: state.tstore.orders,
    saved: state.tstore.saved,
    profile: state.tstore.profile,
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (Profile);
