import React, { Component } from 'react';
import { Text, View, TextInput, ScrollView, YellowBox, ImageBackground, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import {colors} from '../configs/Colors.js';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ProductCard from '../components/ProductCard';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../reducres/tstore.actions';
const { width, height } = Dimensions.get('window');

const navigatorStyle = {
  statusBarColor: colors.white,
  navBarTextFontFamily: 'Dosis-Medium',
  navBarTextFontSize: 30,
  statusBarTextColorScheme: 'dark',
  navigationBarColor: 'transparent',
  navBarTextColor: colors.primary,
  navBarButtonColor: colors.secondry,
  tabBarButtonColor: 'red',
  screenBackgroundColor: colors.white,
  tabBarSelectedButtonColor: 'red',
  tabBarBackgroundColor: 'white',
  topBarElevationShadowEnabled: false,
  navBarHideOnScroll: false,
  tabBarHidden: true,
  drawUnderTabBar: true,
  //drawUnderNavBar: true,
  //navBarTranslucent: true,
  navBarBackgroundColor: colors.white,
  //navBarHidden: false,
  //navBarBlur: false,
  //navBarTransparent: true,
  navBarTitleTextCentered: true
};


const styles = StyleSheet.create({
  container: {
    //flex: 1,
    //marginHorizontal: 10,
    width: width,
    flexDirection: 'column',
    backgroundColor: colors.white,
    justifyContent: 'center',
    alignItems: 'center',
    //marginVertical: 10,
    //borderRadius: 7,
    //alignItems: 'center',
  },
  subcatecont: {
    flex: 1,
    margin: 10,
    flexDirection: "row",
    width: width
  },
  subcatecontbg: {
    backgroundColor: colors.primary,
    borderRadius: 40,
    marginRight: 15
  },
  subcategorybtn: {
    //backgroundColor: colors.white,
    borderRadius: 40,
    margin: 2,
    paddingVertical: 5,
    paddingHorizontal: 10
  },
  subcategoryTitle: {
    //color: colors.primary,
    fontFamily: 'Dosis-Medium',
    fontSize: 20
  },
  productscont: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    alignSelf: 'center',
    alignItems: 'center',
    //width: width,
    //marginHorizontal: 10,
    marginVertical: 10
  }
});
function getItems() {
    var json =  {
      "products": [{ "id": 1,
                 "title": "Red shoe",
                 "price": 120,
                 "image": "https://images.unsplash.com/photo-1505874462322-dfcf87f819a9?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b0b082569a3042da7bb5a6d8bf1c0f7d&auto=format&fit=crop&w=334&q=80"
               },
               { "id": 2,
                 "title": "Handfree",
                 "price": 334,
                 "image": "https://images.unsplash.com/photo-1481207801830-97f0f9a1337e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=be08aede38d12b6e061a1c1a32cc173f&auto=format&fit=crop&w=750&q=80"
               },
               { "id": 3,
                 "title": "Uber",
                 "price": 675,
                 "image": "https://images.unsplash.com/photo-1521001750463-5f3e18f2da2d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=819e9dcb75b84114278a7163ff38a91c&auto=format&fit=crop&w=334&q=80"
               },
               { "id": 4,
                 "title": "Jug",
                 "price": 90,
                 "image": "https://images.unsplash.com/photo-1511001148140-09c2b155f57c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5aadc8ce658d1a8996cdcb0a3144def5&auto=format&fit=crop&w=750&q=80"
               },
               { "id": 5,
                 "title": "Tesla",
                 "price": 90,
                 "image": "https://images.unsplash.com/photo-1522128483605-23607c944cbb?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89c442b8ed5fd57c3a00e59113298a90&auto=format&fit=crop&w=749&q=80"
               }],
    "subcate": [{ "id": 1,
        "title": "Men",
         },
      { "id": 3,
        "title": "Women",
       },
      { "id": 4,
        "title": "Kids",
      },
      { "id": 5,
        "title": "Gifts",
       }]
            }  
        return json;
        }

class Products extends Component {
   constructor(props) {
    super(props);
    this.state = {selectedcate:1 };
    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);
   this.selectSubCategory = this.selectSubCategory.bind(this);
   this._viewProduct = this._viewProduct.bind(this);
   this._goToCart = this._goToCart.bind(this);
   this._nav = this._nav.bind(this);
}
componentDidMount () {
 //this.setState({ });
}
selectSubCategory(subcategoryid) {
  console.log('select sub category',subcategoryid)
  this.setState({ selectedcate: subcategoryid });
}
_viewProduct(productId) {
  console.log('select product')
  this.props.navigator.showModal({
    screen: 'ProductDetails',
    navigatorStyle,
    passProps: {
      productId
    },
      rightButtons: [{
      id: 'cart',
      component: 'CartCar',
      passProps: this._nav(),    
          }],
      leftButtons: [{
      id: 'back',
      icon: require('../images/left.png')
    }],
    animated: true
  });
}
_nav() {
  var total = this.props.cart.reduce(function(_this, val) {
          return (_this+val.quantity)
      }, 0);
    return {
        quantity: total,
        getScreen: this._goToCart.bind(this)
    }
}
_goToCart() {
  console.log('cart');
     this.props.navigator.showModal({
      screen: 'Cart',
      navigatorStyle,
      leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
      animated: true
    });
}

render() { 
    return (
      <View style={styles.container}>
      {getItems().subcate.length > 0 && 
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
         <View style={styles.subcatecont}>
            {getItems().subcate.map((sub) => {
            return (
              <View style={styles.subcatecontbg} key={sub.id}>
          <TouchableOpacity activeOpacity={0.8} 
          style={[styles.subcategorybtn, sub.id == this.state.selectedcate ? {backgroundColor:colors.primary} : {backgroundColor:colors.white}]}
           onPress={this.selectSubCategory.bind(this, sub.id)}>
             <Text style={[styles.subcategoryTitle, sub.id == this.state.selectedcate ? {color:colors.white} : {color:colors.primary}]} numberOfLines={1}>
              {sub.title}
             </Text> 
          </TouchableOpacity>
              </View>
            )})}
         </View>
      </ScrollView>}

      <ScrollView vertical showsVerticalScrollIndicator={false}>
        <View style={styles.productscont}>
        {getItems().products.map((list) => {
          return(
            <ProductCard key={list.id} info={list} viewProduct={this._viewProduct} />
          )})}
          </View>
      </ScrollView>
        </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

function mapStateToProps(state, ownProps) {
  return {
    cart: state.tstore.cart
  }
}
export default connect(mapStateToProps, mapDispatchToProps) (Products);
