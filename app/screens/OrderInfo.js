import React, { Component } from 'react';
import { Text, View, TextInput,Linking, YellowBox, BackHandler, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import {colors} from '../configs/Colors.js';
import { strings } from '../locales/i18n';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    //flex: 1,
    padding: 20,
    height: height*(1.25/2),
    width: width*(2/3),
    flexDirection: 'column',
    backgroundColor: colors.white,
    justifyContent: 'space-between',
    borderRadius: 7,
    //alignItems: 'center',
  },
  header: {
    marginBottom: 1,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  modaltitle: {
    color: colors.ProductTitleDetails,
    fontSize: 18,
    fontFamily: "Dosis-SemiBold",
  },
  orderstatus: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  orderdetails: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'stretch',
  },
  textcont: {
    flexDirection: 'row',
    justifyContent: 'space-between',
    alignItems: 'center',
  },
  textvalue: {
    color: colors.ProductTitleDetails,
    fontFamily: "Dosis-SemiBold",
    fontSize: 18,
  },
  statustext: {
    color: colors.Category,
    fontFamily: "Dosis-SemiBold",
    fontSize: 18,
  },
  storeinfo: {
    flexDirection: 'column',
    marginLeft: 10,
    marginVertical: 8,
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    backgroundColor: colors.white
  },
  time: {
    color: colors.Category,
    fontSize: 20,
    fontFamily: 'Dosis-SemiBold'
  },
  distance: {
    color: colors.Category,
    fontSize: 20,
    fontFamily: 'Dosis-SemiBold'
  },
  address: {
    color: colors.ProductTitleDetails,
    fontSize: 17,
    fontFamily: "Dosis-Meduim",
    marginBottom: 7
  },
  addresscon: {
    //flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
  },
  storeimg: {
    width: 100,
    height: 100,
    borderRadius: 3
  },
  storeimgcon: {
    marginVertical: 5,
    alignItems: 'center',
    justifyContent: 'center',
  },
  storecontainer: {
    flexDirection: 'row',
    //paddingHorizontal: 20,
    //marginTop: 10,
    //height: 130,
    backgroundColor: colors.white
  },
  name: {
    color: colors.ProductTitleDetails,
    fontSize: 25,
    fontFamily: 'Dosis-Medium'
  },
  line: {
    backgroundColor: colors.Category,
    height: 1,
    marginVertical: 10
  },
  ordertitle: {
    color: colors.primary,
    fontSize: 25,
    fontFamily: 'Dosis-Regular'
  },
  textgetdirections: {
    color: colors.primary,
    fontSize: 20,
    fontFamily: "Dosis-SemiBold",
  },
  getdirections: {
    backgroundColor: '#ede9fe',
    borderRadius: 4,
    padding: 10,
    marginVertical: 10
  }
});

class OrderInfo extends Component {
   constructor(props) {
    super(props);
    this.state = {
      message: null
    };
    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);
   this.close = this.close.bind(this);
   this.openGoogleMaps = this.openGoogleMaps.bind(this);
}

openGoogleMaps(url) {
      Linking.canOpenURL(url).then(supported => {
        if (!supported) {
          console.log('Can\'t handle url: ' + url);
        } else {
          return Linking.openURL(url);
        }
      }).catch(err => console.error('An error occurred', err));
}

close() {
  console.log('close feedback');
  this.props.navigator.dismissLightBox();
}

render() {
    return (
      <View style={styles.container}>

      <View style={styles.header}>
           <Text style={styles.modaltitle}>ORDER STATUS</Text>
           <TouchableOpacity activeOpacity={0.8} onPress={this.close} >
              <Icon name="close" size={25} color={colors.ProductTitleDetails} />
           </TouchableOpacity>
      </View>

      <View style={styles.orderstatus}>
          <Icon name="shopping-basket" size={55} color={colors.primary} />
      </View>

       <View style={styles.storecontainer}>
                   <View style={styles.storeimgcon}>
                     <Image style={styles.storeimg} source={{uri: 'https://images.pexels.com/photos/237718/pexels-photo-237718.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940'}}/>
                   </View>
          
                   <View style={styles.storeinfo}>
                          <Text style={styles.name}>Storey</Text>
                          <Text style={styles.time}>10AM · 8PM</Text> 
                          <Text style={styles.distance}>7.5 km</Text> 
                   </View>
        </View>
      
        <View style={styles.addresscon}>
           <Text style={styles.address} numberOfLines={4}>Mohammed Ahmed Ibrahim, Al Manteqah Ath Thamenah, Nasr City, Cairo Governorate</Text>
           <TouchableOpacity activeOpacity={0.8} style={styles.getdirections} 
           onPress={() => this.openGoogleMaps('google.navigation:q=30.065988,31.084692')}>
                       <Text style={styles.textgetdirections}>{strings('confirm.openGoogleMaps')}</Text>
            </TouchableOpacity>
        </View>

      <View style={styles.orderdetails}>
            <View style={styles.textcont}>
               <Text style={styles.statustext}>Status</Text>
               <Text style={styles.textvalue}>{this.props.order.status}</Text>
            </View>
            <View style={styles.textcont}>
               <Text style={styles.statustext}>Date</Text>
               <Text style={styles.textvalue}>{this.props.order.orderdate}</Text>
            </View>
            <View style={styles.textcont}>
               <Text style={styles.statustext}>Total</Text>
               <Text style={styles.textvalue}>{this.props.order.totalprice} EGP</Text>
            </View>
      </View>

      </View>
    );
  }
}

export default OrderInfo;
