import React, { Component } from 'react';
import { Text, YellowBox,TouchableOpacity, StyleSheet,Image, Dimensions, View, ScrollView } from 'react-native';
import Home from './Home';
import {colors} from '../configs/Colors.js';
import { strings } from '../locales/i18n';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../reducres/tstore.actions';

const { width, height } = Dimensions.get('window');

const navigatorStyle = {
  statusBarColor: colors.white,
  navBarTextFontFamily: 'Dosis-Medium',
  navBarTextFontSize: 30,
  statusBarTextColorScheme: 'dark',
  navigationBarColor: colors.white,
  navBarBackgroundColor: colors.white,
  navBarTextColor: colors.primary,
  navBarButtonColor: colors.secondry,
  tabBarButtonColor: 'red',
  screenBackgroundColor: colors.white,
  tabBarSelectedButtonColor: 'red',
  tabBarBackgroundColor: 'white',
  topBarElevationShadowEnabled: false,
  navBarHideOnScroll: false,
  tabBarHidden: true,
  drawUnderTabBar: false,
  navBarTitleTextCentered: true
};


const styles = StyleSheet.create({
  container: {
    backgroundColor:colors.white,
    height: height*(2/3),
    borderBottomRightRadius:4,
    borderTopRightRadius:4,
    width: width*(2/3),
    top: height/6,
    justifyContent: 'center',
  },
  box: {
    marginHorizontal: width*(1/12)
  },
  menu: {
  	fontSize:30,
  	fontFamily: "Dosis-Regular",
  	color: colors.primary,
  },
  profile: {
    marginBottom: 20
  },
  image: {
  	height: 100,
  	width: 100,
  	borderRadius: 6,
  	marginBottom: 15
  },
  name: {
  	fontFamily: "Dosis-Regular",
  	color: colors.secondry,
  	fontSize:25,
  	width: 100
  }
  });
class Drawer extends Component {
   constructor(props) {
    super(props);
    this._goToCart = this._goToCart.bind(this);
    this._goToSearch = this._goToSearch.bind(this);
    this._goToProfile = this._goToProfile.bind(this);
};


    _goToCart() {
       this._toggleDrawer();
       this.props.navigator.showModal({
          screen: 'Cart',
          navigatorStyle,
            rightButtons: [{
            id: 'cart',
            icon: require('../images/cart.png'),
                   
                }],
            leftButtons: [{
            id: 'back',
            icon: require('../images/left.png')
          }],
          animated: true
        });
  }
    _goToSearch() {
       this._toggleDrawer();
       this.props.navigator.showModal({
          screen: 'SearchProduct',
          navigatorStyle,
            rightButtons: [{
            id: 'cart',
            icon: require('../images/cart.png'),
                   
                }],
            leftButtons: [{
            id: 'back',
            icon: require('../images/left.png')
          }],
          animated: true
        });
  }
    _goToProfile(data) {
       this._toggleDrawer();
       this.props.navigator.showModal({
          screen: 'Profile',
          passProps: {tab: data},
          navigatorStyle,
            rightButtons: [{
            id: 'cart',
            component: 'CartCar',
            passProps: this._nav(),
                }],
            leftButtons: [{
            id: 'back',
            icon: require('../images/left.png')
          }],
          animated: true
        });
  }

_nav() {
  var total = this.props.cart.reduce(function(_this, val) {
          return (_this+val.quantity)
      }, 0);
    return {
        quantity: total,
        getScreen: this._goToCart.bind(this)
    }
}
	_toggleDrawer() {
		this.props.navigator.toggleDrawer({
			to: 'closed',
			side: 'left',
			animated: true
		});
  }

  render() {
    return (
    	<View style={styles.container}>
        	<View style={styles.box}>
    	<View style={styles.profile}>
               <Image source={{uri: 'https://images.unsplash.com/photo-1446214814726-e6074845b4ce?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=d09f65c3fa0b677f05dfb40d69ae5b88&auto=format&fit=crop&w=461&q=80'}} style={styles.image} />
    	       <Text numberOfLines={2} style={styles.name}>Hassan elmasry</Text>
    	</View>
    	<View>

    	      <Text style={styles.menu}> • {strings('menu.home')}</Text>
            <TouchableOpacity onPress={ () => this._goToProfile(2)}>
    	      <Text style={styles.menu}> • {strings('menu.profile')}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={ () => this._goToProfile(1)}>
    	      <Text style={styles.menu}> • {strings('menu.saved')}</Text>
            </TouchableOpacity>
            <TouchableOpacity onPress={ () => this._goToProfile(0)}>
    	      <Text style={styles.menu}> • Orders</Text>
            </TouchableOpacity>
    	      <TouchableOpacity onPress={this._goToSearch}>
    	      <Text style={styles.menu}> • {strings('menu.search')}</Text>
    	      </TouchableOpacity>
    	      <TouchableOpacity onPress={this._goToCart}>
    	      <Text style={styles.menu}> • {strings('menu.cart')}</Text>
    	      </TouchableOpacity>
    	      <Text style={styles.menu}> • Sign Out</Text>
        </View>
          </View>
        </View>
       
    	);
}

}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

function mapStateToProps(state, ownProps) {
  return {
    cart: state.tstore.cart
  }
}
export default connect(mapStateToProps, mapDispatchToProps) (Drawer);
