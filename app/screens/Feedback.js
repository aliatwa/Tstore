import React, { Component } from 'react';
import { Text, View, TextInput, YellowBox, BackHandler, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import {colors} from '../configs/Colors.js';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    //flex: 1,
    padding: 20,
    height: height*(1/2),
    width: width*(2/3),
    flexDirection: 'column',
    backgroundColor: colors.white,
    justifyContent: 'space-between',
    borderRadius: 7,
    //alignItems: 'center',
  },
  header: {
    marginBottom: 35,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  modaltitle: {
    color: colors.ProductTitleDetails,
    fontSize: 18,
    fontFamily: "Dosis-SemiBold",
  },
  ques: {
    color: colors.ProductTitleDetails,
    fontSize: 17,
    fontFamily: "Dosis-Meduim",
  },
  sentimentcont: {
    flexDirection: 'column',
    justifyContent: 'flex-start',
    marginBottom: 15
  },
  sentiments: {
    //flex:1,
    flexDirection: 'row',
    justifyContent: 'space-between'
  },
  textinputcont: {
    flex: 1,
    justifyContent: 'flex-start'
  },
  textinput: {
    color: colors.ProductTitleDetails,
    textAlignVertical: "top",
    fontSize: 17,
    fontFamily: "Dosis-Meduim",
    borderWidth: 2,
    borderRadius: 3,
    height: '100%',
    borderColor: colors.feedbackborder,
  },
  sendbtn: {
    backgroundColor: colors.primary,
    borderRadius: 5,
    padding: 10,
  },
  sendtext: {
    color: colors.white,
    fontSize: 18,
    fontFamily: "Dosis-Bold",
  },
  btncontainer: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  }
});





class FeedBack extends Component {
   constructor(props) {
    super(props);
    this.state = {
      message: null, selectedsentiment: 3
    };
    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);
   this.send = this.send.bind(this);
   this.close = this.close.bind(this);
   this.select = this.select.bind(this);
     }

send() {
  console.log('send feedback');
  this.props.navigator.dismissLightBox();
  this.props.onUnmout();
}
close() {
  console.log('close feedback');
  this.props.navigator.dismissLightBox();
}
select(data) {
  console.log('select: ',data);
  this.setState({selectedsentiment: data});
}

componentDidMount() {

}

  render() {
    return (
      <View style={styles.container}>

      <View style={styles.header}>
           <Text style={styles.modaltitle}>SHARE YOUR FEEDBACK</Text>
           <TouchableOpacity activeOpacity={0.8} onPress={this.close} >
              <Icon name="close" size={25} color={colors.ProductTitleDetails} />
           </TouchableOpacity>
      </View>

      <View style={styles.sentimentcont}>
         <Text style={styles.ques}>How satisfied are you?</Text>
         <View style={styles.sentiments}>
            <TouchableOpacity activeOpacity={0.8} onPress={ () => this.select(1)} >
            <Icon name="sentiment-very-dissatisfied" size={60} 
            color={this.state.selectedsentiment == 1 ? colors.activesentiment : colors.deactivesentiment} />
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.8} onPress={ () => this.select(2)} >
            <Icon name="sentiment-dissatisfied" size={60} 
            color={this.state.selectedsentiment == 2 ? colors.activesentiment : colors.deactivesentiment} />
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.8} onPress={ () => this.select(3)} >
            <Icon name="sentiment-satisfied" size={60} 
            color={this.state.selectedsentiment == 3 ? colors.activesentiment : colors.deactivesentiment} />
            </TouchableOpacity>
            <TouchableOpacity activeOpacity={0.8} onPress={ () => this.select(4)} >
            <Icon name="sentiment-very-satisfied" size={60} 
            color={this.state.selectedsentiment == 4 ? colors.activesentiment : colors.deactivesentiment} />
            </TouchableOpacity>
         </View>
      </View>

      <View style={styles.textinputcont}>
      <TextInput
        style={styles.textinput}
        multiline = {true}
        autoFocus = {true}
        underlineColorAndroid='transparent'
        placeholder = 'What do you like most?'
        onChangeText={(message) => this.setState({message})}
        value={this.state.message}
      />
      </View>

      <View style={styles.btncontainer}>
          <TouchableOpacity activeOpacity={0.8} style={styles.sendbtn} onPress={this.send} >
             <Text style={styles.sendtext}>SEND</Text>
          </TouchableOpacity>
      </View>

      </View>
    );
  }
}

export default FeedBack;
