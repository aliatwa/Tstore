import React, { Component } from 'react';
import { Text, View, Alert, TextInput, Linking, ScrollView, YellowBox, ImageBackground, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import { colors } from '../configs/Colors.js';
import { strings } from '../locales/i18n';
import { bindActionCreators } from 'redux';
import BrandCard from '../components/BrandCard';
import { connect } from 'react-redux';
import * as Actions from '../reducres/tstore.actions';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');
import EStyleSheet from 'react-native-extended-stylesheet';
EStyleSheet.build({ $rem: width / 380 });


const navigatorStyle = {
  statusBarColor: colors.white,
  navBarTextFontFamily: 'Dosis-Medium',
  navBarTextFontSize: 30,
  statusBarTextColorScheme: 'dark',
  navigationBarColor: 'transparent',
  navBarTextColor: colors.primary,
  navBarButtonColor: colors.secondry,
  tabBarButtonColor: 'red',
  screenBackgroundColor: colors.white,
  tabBarSelectedButtonColor: 'red',
  tabBarBackgroundColor: 'white',
  topBarElevationShadowEnabled: false,
  navBarHideOnScroll: false,
  tabBarHidden: true,
  drawUnderTabBar: true,
  //drawUnderNavBar: true,
  //navBarTranslucent: true,
  navBarBackgroundColor: colors.white,
  //navBarHidden: false,
  //navBarBlur: false,
  //navBarTransparent: true,
  navBarTitleTextCentered: true
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    width: width,
    flexWrap: 'wrap',
    flexDirection: 'row',
    backgroundColor: colors.white,
    alignSelf: 'center',
    alignItems: 'center',
    justifyContent: 'space-around',
  }

});
function getItems() {
  var json = {
  "brands": [{ "id": 1,
                 "title": "Stabrq",
                 "color": "#32cd32",
                 "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
                 "image": "https://scontent-cai1-1.xx.fbcdn.net/v/t1.0-1/p200x200/14063783_1083961178339966_3300795371085103026_n.png?_nc_cat=0&oh=4d28cab58053bfaaa7b56ad46a9932fa&oe=5B7A3BDD"
               },
               { "id": 2,
                 "title": "Goabas",
                 "color": "#5a1126",
                 "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
                 "image": "https://scontent-cai1-1.xx.fbcdn.net/v/t1.0-1/p200x200/29214836_1645169338885065_5874348580306681856_n.png?_nc_cat=0&oh=d91ccaec796e70c6cee7c3ff7c819cf6&oe=5BB4D4F4"
               },
               { "id": 3,
                 "title": "Khotwh",
                 "color": "#213c4f",
                 "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
                 "image": "https://scontent-cai1-1.xx.fbcdn.net/v/t1.0-1/p200x200/13094198_10154639415000639_4912758056589124617_n.png?_nc_cat=0&oh=e916eedaa0698229b891756caada4f78&oe=5BB95EA7"
               },
               { "id": 4,
                 "title": "IZO",
                 "color": "#010101",
                 "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
                 "image": "https://scontent-cai1-1.xx.fbcdn.net/v/t1.0-1/p200x200/22687951_539006209779229_4551073447838175917_n.jpg?_nc_cat=0&oh=03c419028a0db7d2c0fa8d4e8bbb1b04&oe=5BB90556"
               },
               { "id": 5,
                 "title": "Keswa",
                 "color": "#010214",
                 "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
                 "image": "https://images.unsplash.com/photo-1453834190665-46ff0a1fbd5a?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=02d4a80ebaeb7a0a16734e34fe1aef58&auto=format&fit=crop&w=755&q=80"
               }]
  }
  return json;
}

class Brands extends Component {
  constructor(props) {
    super(props);
    this.state = { mapuri: null, distance: null, category: null, selectedbrand: null };
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);
    this._nav = this._nav.bind(this);
    this._goToCart = this._goToCart.bind(this);
    this._viewBrand = this._viewBrand.bind(this);
  }

  componentDidMount() {
    //this.setState({  });
  }
_viewBrand(brandId) {
     this.props.navigator.showModal({
      screen: 'Brand',
      navigatorStyle,
      title: 'Brand',
      passProps: {
        brandId
      },
       rightButtons: [{
         id: 'cart',
         component: 'CartCar',
         passProps: this._nav(),
       }],
       leftButtons: [{
         id: 'back',
         icon: require('../images/left.png')
       }],
    });
}
  _nav() {
    var total = this.props.cart.reduce(function (_this, val) {
      return (_this + val.quantity)
    }, 0);
    return {
      quantity: total,
      getScreen: this._goToCart.bind(this)
    }
  }
  _goToCart() {
    console.log('cart');
    this.props.navigator.showModal({
      screen: 'Cart',
      navigatorStyle,
      leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
      animated: true
    });
  }

  render() {
    return (
    
      <ScrollView vertical showsVerticalScrollIndicator={false}>
      <View style={styles.container}>
       {getItems().brands.map((brand) => {
        return (
            <BrandCard style={{marginBottom: 25}} key={brand.id} info={brand} viewBrand={this._viewBrand} />
            )})}
      </View>
      </ScrollView>
      
    );
  }
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

function mapStateToProps(state, ownProps) {
  return {
    cart: state.tstore.cart
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Brands);
