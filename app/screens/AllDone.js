import React, { Component } from 'react';
import { Text, View,Animated, YellowBox, BackHandler, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import {colors} from '../configs/Colors.js';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    flex: 1,
    //height: height*(1/2),
    //width: width*(3/4),
    flexDirection: 'column',
    backgroundColor: colors.white,
    justifyContent: 'space-around',
    borderRadius: 5,
    alignItems: 'center',
    alignSelf: 'center',
  },
  tick: {

  },
  AllDone: {
    color: colors.allDone,
    fontSize:16,
    textAlign:'center',
    fontFamily: "Dosis-Bold",
  },
  maintext: {
    color: colors.primary,
    fontSize: 65,
    position: 'relative',
    marginHorizontal: width*0.01,
    textAlign:'center',
    fontFamily: "Dosis-Light",
  },
  backtoshopbtn: {
  },
  backtoshop: {
    color: colors.primary,
    fontSize: 18,
    fontFamily: "Dosis-Medium",
  },
  openyourorder: {
    color: colors.white,
    fontSize: 18,
    fontFamily: "Dosis-Bold",
  },
  openorderbtn: {
    backgroundColor: colors.primary,
    borderRadius: 8,
    padding: 5,
    margin: 7
  },
  btncontainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center'
  }

});

const navigatorStyle = {
  statusBarColor: colors.white,
  statusBarTextColorScheme: 'dark',
  navigationBarColor: colors.white,
  navBarBackgroundColor: colors.white,
  screenBackgroundColor: colors.white,
  tabBarSelectedButtonColor: 'red',
  tabBarBackgroundColor: 'white',
  topBarElevationShadowEnabled: false,
  navBarHideOnScroll: false,
  tabBarHidden: true,
  drawUnderTabBar: true,
 // navBarHidden: true,
  navBarTitleTextCentered: true
};



class AllDone extends Component {
   constructor(props) {
    super(props);
    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);
    
    this.springValue = new Animated.Value(0.5)
    this.goToHome = this.goToHome.bind(this);
    this.goToOrders = this.goToOrders.bind(this);
    this.handleBackPress = this.handleBackPress.bind(this);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
     }

goToHome() {
  this.props.navigator.dismissAllModals({
     animationType: 'none' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
    });
  }
goToOrders() {
  this.props.navigator.dismissAllModals({
    animationType: 'none' // 'none' / 'slide-down' , dismiss animation for the modal (optional, default 'slide-down')
   });
}

componentWillUnmount() {
    this.props.navigator.showModal({
    screen: 'Profile',
    passProps: {tab:0},
    navigatorStyle,
      leftButtons: [{
      id: 'back',
      icon: require('../images/left.png')
    }],
    animated: true
  });
}
onNavigatorEvent(event) {
    switch (event.id) {
      case 'goHome':
        this.goToHome();
        break;
      case 'willAppear':
        console.log('willAppear');
        
        break;
      case 'willDisappear':
        console.log('will DiS Appear');
        //this.goToHome();
        this.backHandler = BackHandler.removeEventListener('hardwareBackPress', this.handleBackPress());
        break;
      default:
        break;
    }
  }

handleBackPress () {
  console.log('back button*******');
  this.goToHome();
    return true;
  }

componentDidMount() {
   this.backHandler = BackHandler.addEventListener('hardwareBackPress', () => this.handleBackPress());
        this.springValue.setValue(0.5)
         Animated.spring(
           this.springValue,
           {
             toValue: 1,
             friction: 1
           }
         ).start()


}

  render() {
    return (
      <View style={styles.container}>
      <Animated.View style={{ transform: [{scale: this.springValue}] }}>
      <Icon name="done" size={60} color={colors.primary} />
      </Animated.View>

      <View>
      <Text style={styles.AllDone}>ALL DONE!</Text>
      <Text style={styles.maintext} numberOfLines={3}>Your order on the way to be processed</Text>
      </View>

      <View style={styles.btncontainer}>
      <TouchableOpacity activeOpacity={0.8} style={styles.openorderbtn} onPress={this.goToOrders} >
      <Text style={styles.openyourorder}>OPEN ORDERS</Text>
      </TouchableOpacity>
      <TouchableOpacity activeOpacity={0.8} style={styles.backtoshopbtn} onPress={this.goToHome} >
      <Text style={styles.backtoshop}>Back TO SHOP</Text>
      </TouchableOpacity>
      </View>

      </View>
    );
  }
}

export default AllDone;
