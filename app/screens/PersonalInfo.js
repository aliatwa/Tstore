import React, { Component } from 'react';
import { Text, View, TextInput, YellowBox, BackHandler, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import {colors} from '../configs/Colors.js';
import { Dropdown } from 'react-native-material-dropdown';
import { TextField } from 'react-native-material-textfield';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    //flex: 1,
    padding: 20,
    height: height*(1.75/3),
    width: width*(2/3),
    flexDirection: 'column',
    backgroundColor: colors.white,
    justifyContent: 'space-between',
    borderRadius: 7,
    //alignItems: 'center',
  },
  header: {
    marginBottom: 35,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  modaltitle: {
    color: colors.ProductTitleDetails,
    fontSize: 18,
    fontFamily: "Dosis-SemiBold",
  },
  inputcont: {
    //flex: 1,
    flexDirection: 'column',
    justifyContent: 'center',
    marginBottom: 15
  },
  gendercont: {
    //flex:1,
    marginTop: 5,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-around'
  },
  gender: {
    color: colors.ProductTitleDetails,
    fontSize: 20,
    fontFamily: "Dosis-Meduim",
    marginLeft: 15
  },
  genderradio: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  textinputcont: {
    //flex: 1,
    height: 70,
    justifyContent: 'flex-start'
  },
  textinput: {
    color: colors.primary,
    textAlignVertical: "top",
    fontSize: 18,
    fontFamily: "Dosis-Meduim",
    borderBottomWidth: 3,
    height: 50,
    borderColor: colors.textinputborder,
  },
  savebtn: {
    backgroundColor: colors.primary,
    borderRadius: 5,
    padding: 10,
  },
  savetext: {
    color: colors.white,
    fontSize: 18,
    fontFamily: "Dosis-Bold",
  },
  btncontainer: {
    marginTop: 20,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  }
});
const gov = [{
  value: '6th of October',
 }, {
  value: 'Al Sharqia',
 }, {
  value: 'Alexandria',
 }, {
  value: 'Aswan',
 }, {
  value: 'Asyut',
 }, {
  value: 'Beheira',
 }, {
  value: 'Beni Suef',
 }, {
  value: 'Cairo',
 }, {
  value: 'Dakahlia',
 }, {
  value: 'Damietta',
 }, {
  value: 'Faiyum',
 }, {
  value: 'Gharbia',
 }, {
  value: 'Giza',
 }, {
  value: 'Helwan',
 }, {
  value: 'Ismailia',
 }, {
  value: 'Kafr el-Sheikh',
 }, {
  value: 'Luxor',
 }, {
  value: 'Matrouh',
 }, {
  value: 'Minya',
 }, {
  value: 'Monufia',
 }, {
  value: 'New Valley',
 }, {
  value: 'North Sinai',
 }, {
  value: 'Port Said',
 }, {
  value: 'Qalyubia',
 }, {
  value: 'Qena',
 }, {
  value: 'Red Sea',
 }, {
  value: 'Sohag',
 }, {
  value: 'South Sinai',
 }, {
  value: 'Suez',
}];

class PersonalInfo extends Component {
   constructor(props) {
    super(props);
    this.state = {
      name: null,
      phone: null,
      address: null,
      gender: null,
      gov: null
    };
    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);
   this.save = this.save.bind(this);
   this.close = this.close.bind(this);
   this.select = this.select.bind(this);
}
componentDidMount () {
 this.setState({ gender: this.props.data.gender,
                 name: this.props.data.name,
                 phone: this.props.data.phone,
                 address: this.props.data.address,
                 gov: this.props.data.gov,
 });
}
save() {
  console.log('save');
  //this.props.navigator.dismissLightBox();
  var data = {}
  data.name = this.state.name;
  data.phone = this.state.phone;
  data.address = this.state.address;
  data.gender = this.state.gender;
  data.gov = this.state.gov;
  this.props.onUnmout(data);
}
close() {
  console.log('close modal');
  this.props.navigator.dismissLightBox();
}
select(data) {
  this.setState({gender: data});
}

render() {
    return (
      <View style={styles.container}>

      <View style={styles.header}>
           <Text style={styles.modaltitle}>UPDATE YOUR PROFILE</Text>
           <TouchableOpacity activeOpacity={0.8} onPress={this.close} >
              <Icon name="close" size={25} color={colors.ProductTitleDetails} />
           </TouchableOpacity>
      </View>

      <View style={styles.inputcont}>
        
      <View style={styles.textinputcont}>
      <TextField
        label='Name'
        fontSize={20}
        textColor={colors.primary}
        tintColor={colors.placeholderTextColor}
        value={this.state.name || ''}
        onChangeText={ (name) => this.setState({ name }) }
      />
      </View>
      <View style={styles.textinputcont}>
      <TextField
        label='Phone'
        fontSize={20}
        textColor={colors.primary}
        tintColor={colors.placeholderTextColor}
        value={this.state.phone || ''}
        onChangeText={ (phone) => this.setState({ phone }) }
      />
      </View>
      <View style={styles.textinputcont}>
      <TextField
        label='Address'
        fontSize={20}
        textColor={colors.primary}
        tintColor={colors.placeholderTextColor}
        value={this.state.address || ''}
        onChangeText={ (address) => this.setState({ address }) }
      />
      </View>
      <Dropdown
        label='Government'
        data={gov}
        selectedItemColor={colors.primary}
        textColor={colors.primary}
        animationDuration={100}
        onChangeText={(gov)=>{this.setState({gov})}}
        value={this.state.gov || gov[0].value}
      />
      <View style={styles.gendercont}>
            <TouchableOpacity activeOpacity={0.8} onPress={ () => this.select('male')} >
            <View style={styles.genderradio}>
            <Icon name={this.state.gender == 'male' ? 'radio-button-checked' : 'radio-button-unchecked'} size={35} 
            color={colors.primary} />
            <Text style={styles.gender}>Male</Text>
            </View>
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.8} onPress={ () => this.select('female')} >
            <View style={styles.genderradio}>
            <Icon name={this.state.gender == 'female' ? 'radio-button-checked' : 'radio-button-unchecked'} size={35}
            color={colors.primary} />
            <Text style={styles.gender}>Female</Text>
            </View>
            </TouchableOpacity>
      </View>

      </View>

      <View style={styles.btncontainer}>
          <TouchableOpacity activeOpacity={0.8} style={styles.savebtn} onPress={this.save} >
             <Text style={styles.savetext}>SAVE</Text>
          </TouchableOpacity>
      </View>

      </View>
    );
  }
}

export default PersonalInfo;
