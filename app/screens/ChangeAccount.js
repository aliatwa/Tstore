import React, { Component } from 'react';
import { Text, View, TextInput, YellowBox, BackHandler, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import {colors} from '../configs/Colors.js';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');

const styles = StyleSheet.create({
  container: {
    padding: 20,
    height: height*(1/3),
    width: width*(2/3),
    flexDirection: 'column',
    backgroundColor: colors.white,
    justifyContent: 'space-between',
    borderRadius: 7,
  },
  header: {
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'space-between'
  },
  modaltitle: {
    color: colors.ProductTitleDetails,
    fontSize: 18,
    fontFamily: "Dosis-SemiBold",
  },
  role: {
    color: colors.ProductTitleDetails,
    fontSize: 20,
    fontFamily: "Dosis-Meduim",
    marginLeft: 15
  },
  roleradio: {
    flexDirection: 'row',
    justifyContent: 'flex-start',
    alignItems: 'center',
  },
  radiocon: {
    //flexDirection: 'row',
    //justifyContent: 'space-between'
  },
  changebtn: {
    backgroundColor: colors.primary,
    borderRadius: 5,
    padding: 10,
  },
  changetext: {
    color: colors.white,
    fontSize: 18,
    fontFamily: "Dosis-Bold",
  },
  btncontainer: {
    marginTop: 15,
    flexDirection: 'row',
    justifyContent: 'flex-end'
  }
});


class ChangeAccount extends Component {
   constructor(props) {
    super(props);
    this.state = {
      selectedrole: 1
    };
    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);
   this.change = this.change.bind(this);
   this.close = this.close.bind(this);
   this.select = this.select.bind(this);
     }

change() {
  console.log('change');
  this.props.navigator.dismissLightBox();
  this.props.onUnmout();
}
close() {
  console.log('close');
  this.props.navigator.dismissLightBox();
}
select(data) {
  console.log('selectRole: ',data);
  this.setState({selectedrole: data});
}

componentWllUnMount() {
  
}

  render() {
    return (
      <View style={styles.container}>

      <View style={styles.header}>
           <Text style={styles.modaltitle}>CHANGE YOUR ACCOUNT</Text>
           <TouchableOpacity activeOpacity={0.8} onPress={this.close} >
              <Icon name="close" size={25} color={colors.ProductTitleDetails} />
           </TouchableOpacity>
      </View>

      <View style={styles.sentimentcont}>
            <TouchableOpacity activeOpacity={0.8} onPress={ () => this.select(1)} >
            <View style={styles.roleradio}>
            <Icon name={this.state.selectedrole == 1 ? 'radio-button-checked' : 'radio-button-unchecked'} size={40} 
            color={colors.primary} />
            <Text style={styles.role}>Customer</Text>
            </View>
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.8} onPress={ () => this.select(2)} >
            <View style={styles.roleradio}>
            <Icon name={this.state.selectedrole == 2 ? 'radio-button-checked' : 'radio-button-unchecked'} size={40}
            color={colors.primary} />
            <Text style={styles.role}>Brand</Text>
            </View>
            </TouchableOpacity>

            <TouchableOpacity activeOpacity={0.8} onPress={ () => this.select(3)} >
            <View style={styles.roleradio}>
            <Icon name={this.state.selectedrole == 3 ? 'radio-button-checked' : 'radio-button-unchecked'} size={40}
            color={colors.primary} />
            <Text style={styles.role}>Store</Text>
            </View>
            </TouchableOpacity>
      </View>

      <View style={styles.btncontainer}>
          <TouchableOpacity activeOpacity={0.8} style={styles.changebtn} onPress={this.change} >
             <Text style={styles.changetext}>CHANGE</Text>
          </TouchableOpacity>
      </View>

      </View>
    );
  }
}

export default ChangeAccount;
