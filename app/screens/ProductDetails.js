import React, { Component } from 'react';
import { Text, YellowBox,ScrollView, TouchableOpacity, StyleSheet, View, Image, Dimensions } from 'react-native';
import PickerCard from '../components/PickerCard';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../reducres/tstore.actions';
import PropTypes from 'prop-types';
import Icon from 'react-native-vector-icons/MaterialIcons';
import {colors} from '../configs/Colors.js';
import Swiper from 'react-native-swiper'

const { width, height } = Dimensions.get('window');
const navigatorStyle = {
  statusBarColor: colors.white,
  navBarTextFontFamily: 'Dosis-Medium',
  navBarTextFontSize: 30,
  statusBarTextColorScheme: 'dark',
  navigationBarColor: colors.white,
  navBarBackgroundColor: colors.white,
  navBarTextColor: colors.primary,
  navBarButtonColor: colors.secondry,
  tabBarButtonColor: 'red',
  screenBackgroundColor: colors.white,
  tabBarSelectedButtonColor: 'red',
  tabBarBackgroundColor: 'white',
  topBarElevationShadowEnabled: false,
  navBarHideOnScroll: false,
  tabBarHidden: true,
  drawUnderTabBar: true,
  navBarTitleTextCentered: true
};
const marg = 40;
function getDetails() {
     var json =  {
      "id": 21345,
      "title": "Jacket",
      "price": 265,
      "quantity": 1,
      "color": "red",
      "brandimg": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg",
      "image": "https://images.unsplash.com/photo-1502326248588-7e9fa7032efa?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f79cd2ef5b77b97337005fb485c67e61&auto=format&fit=crop&w=666&q=80",
      "category": "Jacts & Cotas",
      "description": `Adam Boduch has been involved with large-scale JavaScript development for nearly 10 years. Before moving to the front end, he worked on several large-scale cloud computing products, using Python and Linux.`,
      "images": [{ "id": 1,
                 "image": "https://images.unsplash.com/photo-1502326248588-7e9fa7032efa?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f79cd2ef5b77b97337005fb485c67e61&auto=format&fit=crop&w=666&q=80"
               },
               { "id": 2,
                 "image": "https://images.unsplash.com/photo-1489987707025-afc232f7ea0f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b17958c40e777065643cb65954bede79&auto=format&fit=crop&w=750&q=80"
               },
               { "id": 3,
                 "image": "https://images.unsplash.com/photo-1489987707025-afc232f7ea0f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b17958c40e777065643cb65954bede79&auto=format&fit=crop&w=750&q=80"
               }],
      "options": [
                  {    
                   "id": 1,
                    "type": "color",
                    "label": "Choose your Color",
                    "options":
                  [{
                      "id": 1,
                      "label": "Red",
                      "value": "#fe8371",
                      "type": "color"
                   },
                   {
                      "id": 2,
                      "label": "Purpl",
                      "value": "#d363d3",
                      "type": "color"
                   },
                   {
                      "id": 3,
                      "label": "Yellow",
                      "value": "#fff16a",
                      "type": "color"
                  }]},
                  {    
                    "id": 2,
                    "type": "size",
                    "label": "Choose your Size",
                    "options":
                  [{
                      "id": 1,
                      "label": "Small",
                      "value": "sm",
                      "type": "size"
                   },
                   {
                      "id": 2,
                      "label": "Meduim",
                      "value": "md",
                      "type": "size"
                   },
                   {
                      "id": 3,
                      "label": "Large",
                      "value": "L",
                      "type": "size"
                  }]},
                  {    
                    "id": 3,
                    "type": "custom",
                    "label": "Choose your Custom",
                    "options":
                  [{
                      "id": 1,
                      "label": "Google",
                      "value": "google",
                      "type": "custom"
                   },
                   {
                      "id": 2,
                      "label": "Uber",
                      "value": "uber",
                      "type": "custom"
                   },
                   {
                      "id": 3,
                      "label": "Tesla",
                      "value": "tesla",
                      "type": "custom"
                  }]},
                 ]};
      return json;
               }

const styles = StyleSheet.create({
  subtext:{
    flexDirection: 'row',
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    marginBottom: 20,
    justifyContent: 'space-between',

  },
  cardImage: {
    width: 400,
    height:300,
    borderTopLeftRadius: 10,
    borderBottomLeftRadius: 10,
    marginVertical: 40,
    alignSelf: 'flex-end', 
    justifyContent: 'flex-end'
  },
  uptextbox: {
    width: width/2-15
  },
  title: {
    fontFamily: "Dosis-Bold",
    fontSize: 30,
    color: colors.ProductTitleDetails
  },
  price: {
    color: colors.ProductTitleDetails,
    fontSize: 30,
    fontFamily: "Dosis-SemiBold",
    textAlign: 'center'
  },
  category: {
    color: colors.Category,
    fontSize: 26,
    fontFamily: "Dosis-Regular"
  },
  uptext: { 
    flex: 1,
    flexDirection: 'row',
    marginTop: 50,
    marginHorizontal: 15,
    flexWrap: 'wrap',
  },
  descntainer: {
    marginBottom: 10,
    marginHorizontal: 15,
    flex: 1,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  description: {
    fontFamily: "Dosis-Bold",
    fontSize: 20,
    marginVertical:15,
    color: colors.ProductTitleDetails,
  },
  desctext: {
    fontFamily: "Dosis-Regular",
    fontSize: 20,
    color: colors.secondry,
  },
  image: {
    width:200,
    height: 200,
    marginRight: 20,
    borderRadius: 5
  },
  additem: {
    backgroundColor: colors.primary,
    width: 60,
    height: 60,
    borderRadius: 45,
    marginTop: ((height*(1/3))-30),
    right: 25,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    alignSelf: 'flex-end',
  },
  saveitem: {
    backgroundColor: colors.white,
    width: 50,
    height: 50,
    borderRadius: 45,
    marginTop: ((height*(1/3))-100),
    right: 30,
    justifyContent: 'center',
    alignItems: 'center',
    position: 'absolute',
    alignSelf: 'flex-end',
  },
  addbtn: {
    width: 25,
    height: 25,
  },
  wrapper: {
    flex:1,
    width: width,
    height: height*(1/3),
  },
  gall: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center',
    width: width,
    height: height*0.25,
    backgroundColor: colors.ProductTitle
  },
  yoursize: {
    marginTop: 5,
    marginHorizontal: 10,
    flexDirection: 'column',
    justifyContent: 'flex-start',
    alignItems: 'flex-start',
  },
  pag: {

  }

  });


class ProductDetails extends Component {
  constructor(props) {
    super(props);
    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);
    this.state={options:[], favorite: null};
    this._goToCart = this._goToCart.bind(this);
    this.addItem = this.addItem.bind(this);
    this.saveItem = this.saveItem.bind(this);
    this._selected = this._selected.bind(this);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
  }

onNavigatorEvent(event) { // this is the onPress handler for the two buttons together
   if (event.type == 'NavBarButtonPress') { // this is the event type for button presses
      if (event.id == 'cart') { // this is the same id field from the static navigatorButtons definition
        this._goToCart()
      }
    }
  }
  
_goToCart() {
     this.props.navigator.showModal({
      screen: 'Cart',
      navigatorStyle,
      leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
      animated: true
    });
  }


addItem(data) {
    var newProduct = {};
    newProduct = data;
    newProduct.selectedOptions = this.state.options;
    newProduct.tmpId = Math.floor(Math.random() * 10000);
    var newOptions = newProduct.selectedOptions;
    
    if (this.props.cart.length == 0) {
       this.props.actions.addItem(newProduct); 
       return;
    } 

    var bool = this.props.cart.every(function(ele) {
         return ((JSON.stringify(ele.selectedOptions) != JSON.stringify(newProduct.selectedOptions) 
                && JSON.stringify(ele.id) == JSON.stringify(newProduct.id))
                ||(JSON.stringify(ele.id) != JSON.stringify(newProduct.id)) );
             });
     if (bool) {
       this.props.actions.addItem(newProduct); 
     }
     else {
      this._goToCart();
     }
  }
saveItem() {
  const data = getDetails();
  if (this.props.saved.length == 0) {
    this.props.actions.addSaved(data); 
    this.setState({favorite:true}) 
    return;
  }
  var bool = this.props.saved.every(function(ele) {
      return (JSON.stringify(ele.id) != JSON.stringify(data.id));
          });
  if (bool) {
       console.log('add new saved')
      this.props.actions.addSaved(data);
      this.setState({favorite:true})  
     }
  else {
      console.log('remove new saved')
      this.props.actions.removeSaved(data.id); 
      this.setState({favorite:false})
     }

}
componentDidUpdate(prevProps, prevState) { 
}
componentDidMount() {
  //this.props.actions.resetCart();
   // console.log('===mount>>>',this.state,'<<<mount=====');  
}
componentWillMount() {
  var data = getDetails()
   let options=[];
   for(let i=0; i<data.options.length;i++){
    let newOption = {[data.options[i].type]: data.options[i].options[0]};
     options.push(newOption) 
    }
    this.setState({options})
    var bool = this.props.saved.every(function(ele) {
      return (JSON.stringify(ele.id) != JSON.stringify(data.id));
          });
     if (bool) {
       this.setState({favorite:false})
     }
     else {
       this.setState({favorite:true}) 
     }
}
_selected(option, uuid) {
  let newOption = {[option.type]: option};
  let optionType = newOption[option.type].type;
  let options = [...this.state.options];
  options[uuid] = newOption;
  this.setState({ options });
  }

render() {
    const data = getDetails();
    return (
    <ScrollView vertical showsVerticalScrollIndicator={false}>
    <View>
          <Swiper autoplay={true} style={styles.wrapper} dotColor={colors.white} activeDotColor={colors.primary} showsButtons>
           {data.images.map((list) => {
            return (
            <Image style={styles.gall} key={list.id} source={{uri: list.image}} />
             )})}
           </Swiper>

        <TouchableOpacity activeOpacity={0.8} style={styles.saveitem} onPress={this.saveItem.bind(this, data)}>
          {this.state.favorite ? <Icon name="favorite" size={30} color={colors.favorite} /> 
          : <Icon name="favorite-border" size={30} color={colors.favoritef} />}
        </TouchableOpacity>

        <TouchableOpacity disabled={data.quantity <= 0} activeOpacity={0.8} style={styles.additem} onPress={this.addItem.bind(this, data)}>
          <Icon name="add-shopping-cart" size={30} color={colors.white} />
        </TouchableOpacity>

         <View style={styles.uptext}>
          <Text style={[styles.title,styles.uptextbox]}>{data.title}</Text> 
          <Text style={[styles.price,styles.uptextbox]}>{'$ '+data.price}</Text>
          <Text style={[styles.category,styles.uptextbox]}>{data.category}</Text> 
         </View>


         <View style={styles.descntainer}>
          <Text style={styles.description}>Description</Text>
          <Text style={styles.desctext}>{data.description}</Text> 
         </View>

        {data.options.map((masteroptions, i, arr) => {
         return (
          <View key={masteroptions.id} style={styles.yoursize}>
         <Text style={styles.description}>{masteroptions.label}</Text>
         <ScrollView horizontal showsHorizontalScrollIndicator={false}>
         {masteroptions.options.map((list) => {
         return (
         <PickerCard key={list.id} info={list} uuid={[i]} selectedItem={this.state.options[i]} selected={this._selected}/>
            )})}
         </ScrollView>
         </View>
         )})}

    </View>
    </ScrollView>
    );
  }
}
ProductDetails.propTypes = {
  details: PropTypes.object,
  navigator: PropTypes.object,
  productId: PropTypes.number.isRequired
};

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

function mapStateToProps(state, ownProps) {
  return {
    cart: state.tstore.cart,
    saved: state.tstore.saved
  }
}
export default connect(mapStateToProps, mapDispatchToProps) (ProductDetails);
