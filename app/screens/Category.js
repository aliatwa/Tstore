import React, { Component } from 'react';
import { Text, View, TextInput, ScrollView, YellowBox, ImageBackground, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import {colors} from '../configs/Colors.js';
import { Dropdown } from 'react-native-material-dropdown';
import { TextField } from 'react-native-material-textfield';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../reducres/tstore.actions';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');


const navigatorStyle = {
    statusBarColor: colors.white,
    navBarTextFontFamily: 'Dosis-Medium',
    navBarTextFontSize: 30,
    statusBarTextColorScheme: 'dark',
    navigationBarColor: 'transparent',
    navBarTextColor: colors.primary,
    navBarButtonColor: colors.secondry,
    tabBarButtonColor: 'red',
    screenBackgroundColor: colors.white,
    tabBarSelectedButtonColor: 'red',
    tabBarBackgroundColor: 'white',
    topBarElevationShadowEnabled: false,
    navBarHideOnScroll: false,
    tabBarHidden: true,
    drawUnderTabBar: true,
    //drawUnderNavBar: true,
    //navBarTranslucent: true,
    navBarBackgroundColor: colors.white,
    //navBarHidden: false,
    //navBarBlur: false,
    //navBarTransparent: true,
    navBarTitleTextCentered: true
  };

const styles = StyleSheet.create({
  container: {
    //flex: 1,
    padding: 10,
    width: width,
    flexDirection: 'column',
    backgroundColor: colors.white,
    justifyContent: 'space-between',
    //marginVertical: 10,
    //borderRadius: 7,
    //alignItems: 'center',
  },
  box: {
    marginBottom: 10,
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    height: 120,
    //marginHorizontal: ,
    width: width-20,
    //backgroundColor: colors.primary,
  },
  categoryImage: {
    height: 120,
    width: width-20,
    alignItems: 'center',
    justifyContent: 'center',
  },
  categoryTitle: {
      color: colors.white,
      fontFamily: "Dosis-Bold",
      fontSize: 35,
      textAlign: 'center'
  },
  categoryContainer: {
    flexDirection: 'column',
    alignItems: 'center',
    justifyContent: 'center',
  },
  over: {
    backgroundColor: 'rgba(0,0,0,0.155)',
    borderRadius: 5,
    position: 'absolute',
    top: 0,
    left: 0,
    right: 0,
    bottom: 0
  }

});
function getItems() {
    var json =  {
    "categories": [{ "id": 1,
        "title": "Toys",
        "image": "https://images.unsplash.com/photo-1484824823018-c36f00489002?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=45f6c16ed89b6c5b4ad7b4e06c26d0ff&auto=format&fit=crop&w=750&q=80"
      },
      
      { "id": 3,
        "title": "Clothing",
        "image": "https://scontent.xx.fbcdn.net/v/t45.1600-4/33133963_23842809724670274_5034725823043600384_n.png?_nc_cat=0&oh=a45a704a2ce9876d1db4b1b336f75cd9&oe=5B941D76"
      },
      { "id": 4,
        "title": "Perfume",
        "image": "https://images.unsplash.com/photo-1519493490532-9641938d51b2?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=e5c66943eed79118d407f475a5837bae&auto=format&fit=crop&w=750&q=80"
      },
      { "id": 5,
        "title": "Gifts",
        "image": "https://scontent.xx.fbcdn.net/v/t45.1600-4/33028753_23842873883990676_7159152239762735104_n.png?_nc_cat=0&oh=f5f0883e456718b818d14b44b7dd0878&oe=5B7BE9D8"
      }]
            }  
        return json;
        }

class Category extends Component {
   constructor(props) {
    super(props);
   // this.state = { };
    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);
   this.selectCategory = this.selectCategory.bind(this);
   this._nav = this._nav.bind(this);
   this._goToCart = this._goToCart.bind(this);
}
componentDidMount () {
 //this.setState({ });
}

selectCategory(categoryid, title) {
  console.log('select cate')
  this.props.navigator.showModal({
    screen: 'Products',
    animated: true,
    navigatorStyle,
    title: title,
    passProps: {
    },
    rightButtons: [{
      id: 'cart',
      component: 'CartCar',
      passProps: this._nav(),    
          }],
      leftButtons: [{
      id: 'back',
      icon: require('../images/left.png')
    }],
  });
}

_nav() {
    var total = this.props.cart.reduce(function(_this, val) {
            return (_this+val.quantity)
        }, 0);
      return {
          quantity: total,
          getScreen: this._goToCart.bind(this)
      }
}
_goToCart() {
    console.log('cart');
       this.props.navigator.showModal({
        screen: 'Cart',
        navigatorStyle,
        leftButtons: [{
          id: 'back',
          icon: require('../images/left.png')
        }],
        animated: true
      });
}
render() { 
    return (
        <ScrollView vertical showsVerticalScrollIndicator={false}>
        <View style={styles.container}>
        {getItems().categories.map((info) => {
          return(
          <View style={styles.box} key={info.id}>
             <TouchableOpacity activeOpacity={0.8} onPress={this.selectCategory.bind(this, info.id, info.title)}>
                <View style={styles.categoryContainer}>
       
                 <ImageBackground source={{uri: info.image}} borderRadius={5} style={styles.categoryImage}>
                  <View style={styles.over}/>
                  <Text style={styles.categoryTitle} numberOfLines={1}>
                   {info.title}
                  </Text> 
                 </ImageBackground>
                 

                </View>
             </TouchableOpacity>
          </View>
          )})}
          </View>
        </ScrollView>
    );
  }
}
function mapDispatchToProps(dispatch) {
    return {
      actions: bindActionCreators(Actions, dispatch)
    };
  }
  
  function mapStateToProps(state, ownProps) {
    return {
      cart: state.tstore.cart
    }
  }
export default connect(mapStateToProps, mapDispatchToProps) (Category);
