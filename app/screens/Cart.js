import React, { Component } from 'react';
import { Text, View, YellowBox,StyleSheet,Dimensions, Image, ScrollView, TouchableOpacity } from 'react-native';
import { strings } from '../locales/i18n';
import {colors} from '../configs/Colors.js';
import CartCard from '../components/CartCard';
import { bindActionCreators } from 'redux';
import * as Actions from '../reducres/tstore.actions';
import { connect } from 'react-redux';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');

const navigatorStyle = {
  statusBarColor: colors.white,
  statusBarTextColorScheme: 'dark',
  navigationBarColor: colors.white,
  navBarBackgroundColor: colors.white,
  screenBackgroundColor: colors.white,
  tabBarSelectedButtonColor: 'red',
  tabBarBackgroundColor: 'white',
  topBarElevationShadowEnabled: false,
  navBarHideOnScroll: false,
  tabBarHidden: true,
  drawUnderTabBar: true,
 // navBarHidden: true,
  navBarTitleTextCentered: true
};


const styles = StyleSheet.create({
  container:{
    flex: 1,
    justifyContent: 'flex-start',
  },
  yourcart: {
    marginVertical: 20,
    marginHorizontal: 90,
    color: colors.secondry,
    fontSize: 28,
    fontFamily: "Dosis-Medium",
    textAlign: 'left'
  },
  placeorder: {
    color: colors.white,
    fontSize: 23,
    fontFamily: "Dosis-SemiBold",
  },
  order: {
    backgroundColor: colors.primary,
    alignItems: 'center',
    justifyContent: 'center',
    marginHorizontal: (width*1/6),
    marginVertical: 25,
    borderRadius: 45,
    width: width*(2/3),
    paddingVertical: 20
  }

});


function getDetails() {
     var json =  {
           "cart": [{"id": 1,
                                 "title": "Reversible Anroka",
                                 "price": 120.24,
                                 "quantity": 1,
                                 "image": "https://images.unsplash.com/photo-1502326248588-7e9fa7032efa?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f79cd2ef5b77b97337005fb485c67e61&auto=format&fit=crop&w=666&q=80",
                    },
                    {"id": 2,
                                 "title": "Jaket",
                                 "price": 99,
                                 "quantity": 2,
                                 "image": "https://images.unsplash.com/photo-1502326248588-7e9fa7032efa?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f79cd2ef5b77b97337005fb485c67e61&auto=format&fit=crop&w=666&q=80",
                    },
                    {"id": 3,
                                 "title": "Jaket",
                                 "price": 99,
                                 "quantity": 2,
                                 "image": "https://images.unsplash.com/photo-1502326248588-7e9fa7032efa?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f79cd2ef5b77b97337005fb485c67e61&auto=format&fit=crop&w=666&q=80",
                    },
                    {"id": 4,
                                 "title": "Jaket",
                                 "price": 99,
                                 "quantity": 2,
                                 "image": "https://images.unsplash.com/photo-1502326248588-7e9fa7032efa?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f79cd2ef5b77b97337005fb485c67e61&auto=format&fit=crop&w=666&q=80",
                    },
                    {"id": 5,
                                 "title": "Jaket",
                                 "price": 99,
                                 "quantity": 2,
                                 "image": "https://images.unsplash.com/photo-1502326248588-7e9fa7032efa?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=f79cd2ef5b77b97337005fb485c67e61&auto=format&fit=crop&w=666&q=80",
                    } 
                    ]};
      return json;
               }

class Cart extends Component {
   constructor() {
    super();
  this.state = {total: 0};
    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
     'Warning: componentWillReceiveUpdate is deprecated',
     'Warning: componentDidUpdate is deprecated',
     'Warning: componentWillUpdate is deprecated',
   ]);
        this._removeItem = this._removeItem.bind(this);
        this.allDone = this.allDone.bind(this);
        this.placeOrder = this.placeOrder.bind(this);
        this._minus = this._minus.bind(this);
        this._plus = this._plus.bind(this);
        this.orderValue = this.orderValue.bind(this);
}



allDone() {
   

   /*this.props.navigator.showModal({
      screen: 'AllDone',
      navigatorStyle,
      animated: true
    });*/
  }

_removeItem(data) {
  this.props.actions.removeItem(data);
console.log('remove Item');
}
placeOrder() {
   //this.allDone();
   this.props.navigator.showModal({
      screen: 'Confirm',
      navigatorStyle,
        leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
      animated: true
    });
   //this.props.actions.resetCart();
  //this.props.actions.placeOrder(data);
 // console.log('placeOrder');
}
_minus(itemId) {
  var quantityM = this.props.cart.find(function(item){
    return item.tmpId == itemId;
});
if (quantityM.quantity >= 2) { 
            this.props.actions.minusQuantity(itemId)
 }

}
_plus(itemId) {
  
  var quantityP = this.props.cart.find(function(item){
    return item.tmpId == itemId;
});
if (quantityP.quantity <= 4) { 
             this.props.actions.plusQuantity(itemId);
 }
}

componentDidMount () {
  this.orderValue()
}

componentDidUpdate(previousProps, previousState) {
   if(previousProps.cart !== this.props.cart) {
     this.orderValue()
          }
}
orderValue() {
  var total = this.props.cart.reduce(function(_this, val) {
          return (_this+(val.quantity*val.price))
      }, 0);
    this.setState({ total: total.toFixed(2)});
}

  render() {
    return (
      <View style={styles.container}>
       <Text style={styles.yourcart}>{strings('cart.yourcart')}</Text>
       <ScrollView vertical showsVerticalScrollIndicator={false}>
        {this.props.cart.length > 0 ? 
       (this.props.cart.map((items) => {
               return(
               <CartCard key={items.id} info={items} minus={this._minus} plus={this._plus} removeItem={this._removeItem} />
              )})
        ):(<Text style={styles.yourcart}>Your Cart is Empty</Text>)
         }

       </ScrollView>
    <TouchableOpacity activeOpacity={0.5} style={styles.order} onPress={this.placeOrder.bind(this)}>
           <Text style={styles.placeorder}>{strings('cart.placeorder', { price: this.state.total })}</Text>
    </TouchableOpacity>
      </View>
    );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

function mapStateToProps(state, ownProps) {
  return {
    cart: state.tstore.cart
  }
}

export default connect(mapStateToProps, mapDispatchToProps) (Cart);
