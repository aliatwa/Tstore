import React, { Component } from 'react';
import { Text, View, Alert, TextInput, Linking, ScrollView, YellowBox, ImageBackground, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import { colors } from '../configs/Colors.js';
import { strings } from '../locales/i18n';
import { bindActionCreators } from 'redux';
import ProductCard from '../components/ProductCard';
import { connect } from 'react-redux';
import * as Actions from '../reducres/tstore.actions';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');
import EStyleSheet from 'react-native-extended-stylesheet';
EStyleSheet.build({ $rem: width / 380});


const navigatorStyle = {
  statusBarColor: colors.white,
  navBarTextFontFamily: 'Dosis-Medium',
  navBarTextFontSize: 30,
  statusBarTextColorScheme: 'dark',
  navigationBarColor: 'transparent',
  navBarTextColor: colors.primary,
  navBarButtonColor: colors.secondry,
  tabBarButtonColor: 'red',
  screenBackgroundColor: colors.white,
  tabBarSelectedButtonColor: 'red',
  tabBarBackgroundColor: 'white',
  topBarElevationShadowEnabled: false,
  navBarHideOnScroll: false,
  tabBarHidden: true,
  drawUnderTabBar: true,
  //drawUnderNavBar: true,
  //navBarTranslucent: true,
  navBarBackgroundColor: colors.white,
  //navBarHidden: false,
  //navBarBlur: false,
  //navBarTransparent: true,
  navBarTitleTextCentered: true
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    width: width,
    flexDirection: 'column',
    backgroundColor: colors.white,
    justifyContent: 'flex-start'
  },
  usermetacontainer: {
    flexDirection: 'row',
    justifyContent: 'space-around',
    alignItems: 'center'
  },
  productnumcont: {
    flexDirection: 'column',
    justifyContent: 'center',
    alignSelf: 'center',
    alignItems: 'center'
  },
  productsnum: {
    fontSize: 33,
    fontFamily: "Dosis-Medium",
    color: colors.ProductTitleDetails
  },
  productsnumlabel: {
    fontSize: 23,
    fontFamily: "Dosis-Medium",
    color: colors.Category
  },
  mapimg: {
    width: 100,
    height: 100,
    borderRadius: 5
  },
  storecontainer: {
    flexDirection: 'row',
    paddingHorizontal: 20,
    marginTop: 10,
    height: 130,
    justifyContent: 'space-between',
    alignItems: 'center'
  },
  storeimg: {
    width: 100,
    height: 100,
    borderRadius: 8
  },
  storeimgcon: {
    marginVertical: 10,
    alignItems: 'center',
    justifyContent: 'center',
  },
  storeinfo: {
    flexDirection: 'column',
    marginLeft: 10,
    //marginVertical: 8,
    alignItems: 'flex-start',
    justifyContent: 'space-around',
    backgroundColor: colors.white
  },
  name: {
    color: colors.ProductTitleDetails,
    fontSize: 30,
    fontFamily: 'Dosis-Medium'
  },
  time: {
    color: colors.Category,
    fontSize: 20,
    fontFamily: 'Dosis-SemiBold'
  },
  distance: {
    color: colors.Category,
    fontSize: 20,
    fontFamily: 'Dosis-SemiBold'
  },
  category: {
    color: colors.ProductTitleDetails,
    fontSize: 20,
    fontFamily: 'Dosis-SemiBold'
  },
  address: {
    color: colors.Category,
    fontSize: 17,
    fontFamily: "Dosis-Regular"
  },
  metacont: {
    flexDirection: 'row'
  },
  productscont: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    alignSelf: 'center',
    alignItems: 'center',
    //width: width,
    //marginHorizontal: 10,
    marginBottom: 5
  },
  line: {
    backgroundColor: colors.line+'70',
    height: 1,
    marginVertical: 8,
    marginHorizontal: 20
  },
  brandcont: {
    flex: 1,
    marginHorizontal: 10,
    marginBottom: 10,
    flexDirection: "row",
    width: width
  },
  brandimg: {
    width: 50,
    height: 50,
    borderRadius: 45
  },
  brandbtn: {
    borderRadius: 45,
    marginLeft: 10,
    padding: 4
  }

});
function getItems() {
  var json = {
    "brands": [{
      "id": 1,
      "name": "Stabrq",
      "brandimg": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "brandimg": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "brandimg": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
     ],
    "products": [{
      "id": 1,
      "title": "Red shoe",
      "price": 120,
      "brandimg": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg",
      "image": "https://images.unsplash.com/photo-1505874462322-dfcf87f819a9?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b0b082569a3042da7bb5a6d8bf1c0f7d&auto=format&fit=crop&w=334&q=80"
    },
    {
      "id": 2,
      "title": "Handfree",
      "price": 334,
      "brandimg": "https://scontent-cai1-1.cdninstagram.com/vp/15694667eede1ccdda51dd094b523398/5BB50BD5/t51.2885-19/s150x150/28750886_600390463660226_2334580953218809856_n.jpg",
      "image": "https://images.unsplash.com/photo-1481207801830-97f0f9a1337e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=be08aede38d12b6e061a1c1a32cc173f&auto=format&fit=crop&w=750&q=80"
    },
    {
      "id": 3,
      "title": "Uber",
      "price": 675,
      "brandimg": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg",
      "image": "https://images.unsplash.com/photo-1521001750463-5f3e18f2da2d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=819e9dcb75b84114278a7163ff38a91c&auto=format&fit=crop&w=334&q=80"
    },
    {
      "id": 4,
      "title": "Jug",
      "price": 90,
      "brandimg": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg",
      "image": "https://images.unsplash.com/photo-1511001148140-09c2b155f57c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5aadc8ce658d1a8996cdcb0a3144def5&auto=format&fit=crop&w=750&q=80"
    },
    {
      "id": 5,
      "title": "Tesla",
      "price": 90,
      "brandimg": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg",
      "image": "https://images.unsplash.com/photo-1522128483605-23607c944cbb?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89c442b8ed5fd57c3a00e59113298a90&auto=format&fit=crop&w=749&q=80"
    }]
  }
  return json;
}

class Store extends Component {
  constructor(props) {
    super(props);
    this.state = { mapuri: null, distance: null, category: null, selectedbrand: null};
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);
    this._nav = this._nav.bind(this);
    this._goToCart = this._goToCart.bind(this);
    this._viewProduct = this._viewProduct.bind(this);
    this.gotogooglemap = this.gotogooglemap.bind(this);
    this.selectBrand = this.selectBrand.bind(this);
}

componentDidMount() {
  this.setState({ distance: '7.5 km', category: 'Clothing  ·  Shoes' });
  this.getDirections("30.509936, 31.151507", "30.465988, 31.184692")
}

async getDirections(startLoc, storeLoc) {
  var markers = 'markers=color:0x7d51ff' + encodeURI('|') + 'label:T' + encodeURI('|' + storeLoc);
  var mapuri = `https://maps.googleapis.com/maps/api/staticmap?size=100x100&scale=1&maptype=roadmap&${markers}`;
  this.setState({ mapuri })
}

gotogooglemap(url) {
  Alert.alert(
  'Store Location',
  'Mohammed Ahmed Ibrahim, Al Manteqah Ath Thamenah, Nasr City, Cairo Governorate',
  [
    {text: 'Open in Google Maps', onPress: () => {
      Linking.canOpenURL(url).then(supported => {
        if (!supported) {
          console.log('Can\'t handle url: ' + url);
        } else {
          return Linking.openURL(url);
        }
      }).catch(err => console.error('An error occurred', err));
    }}
    ,
    {text: 'Cancel', onPress: () => console.log('OK Pressed')},
  ],
  { cancelable: true }
 )

}

selectBrand(item) {
  if (this.state.selectedbrand == item) {
    this.setState({ selectedbrand: null });
    return;
  }
  this.setState({ selectedbrand: item});
}
_viewProduct(productId) {
    this.props.navigator.showModal({
      screen: 'ProductDetails',
      navigatorStyle,
      passProps: {
        productId
      },
      rightButtons: [{
        id: 'cart',
        component: 'CartCar',
        passProps: this._nav(),
      }],
      leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
      animated: true
    });
}

_nav() {
    var total = this.props.cart.reduce(function (_this, val) {
      return (_this + val.quantity)
    }, 0);
    return {
      quantity: total,
      getScreen: this._goToCart.bind(this)
    }
}
_goToCart() {
    console.log('cart');
    this.props.navigator.showModal({
      screen: 'Cart',
      navigatorStyle,
      leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
      animated: true
    });
}

render() {
    return (
      <ScrollView vertical showsVerticalScrollIndicator={false}>
        <View style={styles.container}>

          <View style={styles.storecontainer}>
            <View style={styles.metacont}>
            <View style={styles.storeimgcon}>
              <Image style={styles.storeimg} source={{ uri: 'https://images.pexels.com/photos/237718/pexels-photo-237718.jpeg?auto=compress&cs=tinysrgb&dpr=2&h=650&w=940' }} />
            </View>
            <View style={styles.storeinfo}>
              <Text style={styles.name}>Storey</Text>
              <Text style={styles.category}>{this.state.category}</Text>
              <Text style={styles.distance}>{this.state.distance}</Text>
            </View>
            </View>
            <TouchableOpacity activeOpacity={0.8} onPress={() => this.gotogooglemap('google.navigation:q=30.065988,31.084692')} >
              <Image style={styles.mapimg} source={{ uri: this.state.mapuri }} />
            </TouchableOpacity>
          </View>

          <View style={styles.line} />

          {getItems().brands.length > 0 &&
            <ScrollView horizontal showsHorizontalScrollIndicator={false}>
              <View style={styles.brandcont}>
                {getItems().brands.map((brand) => {
                  return (
                    <View style={styles.brandcontbg} key={brand.id}>
                      <TouchableOpacity activeOpacity={0.8}
                        style={[styles.brandbtn, brand.id == this.state.selectedbrand ? { backgroundColor: colors.primary } : { backgroundColor: colors.white }]}
                        onPress={this.selectBrand.bind(this, brand.id)}>
                        <Image style={styles.brandimg} source={{ uri: brand.brandimg }} />
                      </TouchableOpacity>
                    </View>
                  )
                })}
              </View>
            </ScrollView>}


          <ScrollView vertical showsVerticalScrollIndicator={false}>
            <View style={styles.productscont}>
              {getItems().products.map((list) => {
                return (
                  <ProductCard key={list.id} info={list} viewProduct={this._viewProduct} />
                )
              })}
            </View>
          </ScrollView>

        </View>
      </ScrollView>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

function mapStateToProps(state, ownProps) {
  return {
    cart: state.tstore.cart
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Store);
