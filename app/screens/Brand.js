import React, { Component } from 'react';
import { Text, Alert, View, TextInput, ScrollView, YellowBox, ImageBackground, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import { colors } from '../configs/Colors.js';
import { Dropdown } from 'react-native-material-dropdown';
import { TextField } from 'react-native-material-textfield';
import { bindActionCreators } from 'redux';
import ProductCard from '../components/ProductCard';
import StoreCardInList from '../components/StoreCardInList';
import ScrollableTabView from 'react-native-scrollable-tab-view';
import DefaultTabBar from '../components/DefaultTabBar';
import { connect } from 'react-redux';
import * as Actions from '../reducres/tstore.actions';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');


const navigatorStyle = {
  statusBarColor: colors.white,
  navBarTextFontFamily: 'Dosis-Medium',
  navBarTextFontSize: 30,
  statusBarTextColorScheme: 'dark',
  navigationBarColor: 'transparent',
  navBarTextColor: colors.primary,
  navBarButtonColor: colors.secondry,
  tabBarButtonColor: 'red',
  screenBackgroundColor: colors.white,
  tabBarSelectedButtonColor: 'red',
  tabBarBackgroundColor: 'white',
  topBarElevationShadowEnabled: false,
  navBarHideOnScroll: false,
  tabBarHidden: true,
  drawUnderTabBar: true,
  //drawUnderNavBar: true,
  //navBarTranslucent: true,
  navBarBackgroundColor: colors.white,
  //navBarHidden: false,
  //navBarBlur: false,
  //navBarTransparent: true,
  navBarTitleTextCentered: true
};

const styles = StyleSheet.create({
  container: {
    flex: 1,
    padding: 10,
    width: width,
    flexDirection: 'column',
    backgroundColor: colors.white,
    justifyContent: 'space-between',
  },
  brandcont: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'flex-start',
    height: 120,
    marginHorizontal: width*(0.125/3)
  },
  brandimg: {
    width: 80,
    height: 80,
    borderRadius: 45
  },
  brandinfo: {
    marginLeft: 15,
    height: 80,
    flexDirection: 'column',
    justifyContent: 'space-around',
    alignItems: 'flex-start',
  },
  rowtitle: {
    flexDirection: 'row',
    justifyContent: 'center',
    alignItems: 'center',
  },
  btnlocation: {
    flexDirection: 'row',
    alignItems: 'center',
    justifyContent: 'center',
    paddingHorizontal: 10,
    paddingVertical: 3,
    borderRadius: 40,
    backgroundColor: colors.white
  },
  touch: {
    backgroundColor: colors.background,
    padding: 2,
    borderRadius: 40,
    marginLeft: 25,
  },
  location: {
    marginLeft: 3,
    fontSize: 20,
    fontFamily: "Dosis-Medium",
    color: colors.primary
  },
  name: {
    color: colors.ProductTitleDetails,
    fontSize: 30,
    fontFamily: 'Dosis-Medium'
  },
  category: {
    color: colors.Category,
    fontSize: 20,
    fontFamily: 'Dosis-SemiBold'
  },
  line: {
    backgroundColor: colors.line+'80',
    height: 1,
    marginVertical: 8,
    marginHorizontal: 20
  },
  productscont: {
    flex: 1,
    flexDirection: 'row',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    alignSelf: 'center',
    alignItems: 'center',
    marginVertical: 10
  },
  storecont: {
    flex: 1,
    flexDirection: 'column',
  },
  contentContainer: { 
    flex: 1,
  },
  textStyle: {
		color: colors.secondry,
		paddingTop: 10,
    fontSize: 16,
    fontFamily: "Dosis-SemiBold"
  },
  underlineStyle: {
    backgroundColor: colors.primary
  },
  tabBar: {
    backgroundColor: colors.white,
    marginHorizontal: 40,
    marginBottom: 10
	},

});
function getItems() {
  var json = {
    "stores": [{
      "id": 1,
      "title": "Giga",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 4,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1477120206578-46b3ca98a4e2?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=66315784054a15f1c2d79fc9ca929820&auto=format&fit=crop&w=334&q=80"
    },
    {
      "id": 2,
      "title": "Media",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1496250838193-02a942028e18?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=539b7735765bf84c978e6b9ad4133c59&auto=format&fit=crop&w=750&q=80"
    },
    {
      "id": 3,
      "title": "Omega",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1514792368985-f80e9d482a02?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b067b70aaa4390e9c1ad2133f406a377&auto=format&fit=crop&w=750&q=80"
    },
    {
      "id": 4,
      "title": "Memoa",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1445205170230-053b83016050?ixlib=rb-0.3.5&s=e5bd33f231c2af32a7bda0b4b070c1d3&auto=format&fit=crop&w=751&q=80"
    },
    {
      "id": 5,
      "title": "Tesla",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1441984443719-15c73b016ad1?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=63708ad8de717fa98f3bcfbbe506b283&auto=format&fit=crop&w=750&q=80"
    }],
   "products": [{ "id": 1,
                 "title": "Red shoe",
                 "price": 120,
                 "image": "https://images.unsplash.com/photo-1505874462322-dfcf87f819a9?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b0b082569a3042da7bb5a6d8bf1c0f7d&auto=format&fit=crop&w=334&q=80"
               },
               { "id": 2,
                 "title": "Handfree",
                 "price": 334,
                 "image": "https://images.unsplash.com/photo-1481207801830-97f0f9a1337e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=be08aede38d12b6e061a1c1a32cc173f&auto=format&fit=crop&w=750&q=80"
               },
               { "id": 3,
                 "title": "Uber",
                 "price": 675,
                 "image": "https://images.unsplash.com/photo-1521001750463-5f3e18f2da2d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=819e9dcb75b84114278a7163ff38a91c&auto=format&fit=crop&w=334&q=80"
               },
               { "id": 4,
                 "title": "Jug",
                 "price": 90,
                 "image": "https://images.unsplash.com/photo-1511001148140-09c2b155f57c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5aadc8ce658d1a8996cdcb0a3144def5&auto=format&fit=crop&w=750&q=80"
               },
               { "id": 5,
                 "title": "Tesla",
                 "price": 90,
                 "image": "https://images.unsplash.com/photo-1522128483605-23607c944cbb?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89c442b8ed5fd57c3a00e59113298a90&auto=format&fit=crop&w=749&q=80"
               }],
    
  }
  return json;
}

class Brand extends Component {
  constructor(props) {
    super(props);
    this.state = {tab: 0 };
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);
    this.gotogooglemap = this.gotogooglemap.bind(this);
    this._viewProduct = this._viewProduct.bind(this);
    this._viewStore = this._viewStore.bind(this);
    this._nav = this._nav.bind(this);
    this._goToCart = this._goToCart.bind(this);
  }
  componentDidMount() {
    //this.setState({ });
  }

gotogooglemap(url) {
  Alert.alert(
  'Brand Location',
  'Mohammed Ahmed Ibrahim, Al Manteqah Ath Thamenah, Nasr City, Cairo Governorate',
  [
    {text: 'Open in Google Maps', onPress: () => {
      Linking.canOpenURL(url).then(supported => {
        if (!supported) {
          console.log('Can\'t handle url: ' + url);
        } else {
          return Linking.openURL(url);
        }
      }).catch(err => console.error('An error occurred', err));
    }}
    ,
    {text: 'Cancel', onPress: () => console.log('OK Pressed')},
  ],
  { cancelable: true }
 )

}
_nav() {
    var total = this.props.cart.reduce(function (_this, val) {
      return (_this + val.quantity)
    }, 0);
    return {
      quantity: total,
      getScreen: this._goToCart.bind(this)
    }
  }
_goToCart() {
    console.log('cart');
    this.props.navigator.showModal({
      screen: 'Cart',
      navigatorStyle,
      leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
      animated: true
    });
}
  _viewProduct(id, viewData) {
    console.log('data: ', viewData)
  this.props.navigator.showLightBox({
    screen: "ProductMini",
    passProps: {viewData },
    style: {
      backgroundBlur: "dark",
      backgroundColor: "#00000080",
      tapBackgroundToDismiss: true
    },
    adjustSoftInput: "resize", // android only, adjust soft input, modes: 'nothing', 'pan', 'resize', 'unspecified' (optional, default 'unspecified')
  });
}

_viewStore(storeId) {
    this.props.navigator.showModal({
      screen: 'Store',
      navigatorStyle,
      passProps: {
        storeId
      },
      rightButtons: [{
        id: 'cart',
        component: 'CartCar',
        passProps: this._nav(),
      }],
      leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
      animated: true
    });
  }
render() {
    return (
        <View style={styles.container}>

          <View style={styles.brandcont}>
                 <View style={styles.brandimgcon}>
                     <Image style={styles.brandimg} source={{ uri: 'https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg' }} />
                 </View>
               <View style={styles.brandinfo}>
                <View style={styles.rowtitle}>
                 <Text style={styles.name}>Stabrq</Text>
                 <TouchableOpacity style={styles.touch} activeOpacity={0.8} onPress={() => this.gotogooglemap('google.navigation:q=30.065988,31.084692')} >
                  <View style={styles.btnlocation}>
                    <Icon name="place" size={20} color={colors.primary} />
                    <Text style={styles.location}>Location</Text>
                  </View>
                 </TouchableOpacity>
                </View>
                 <Text style={styles.category}>Clothing · Toys</Text>
               </View>
          </View>


          <View style={styles.contentContainer}>
						<ScrollableTabView initialPage={this.state.tab}
							renderTabBar={() => (
								<DefaultTabBar
									textStyle={styles.textStyle}
									underlineStyle={styles.underlineStyle}
									style={styles.tabBar}
								/>
							)}>

              <ScrollView contentContainerStyle={{flex: 0}} vertical tabLabel="STORES" showsVerticalScrollIndicator={false}>
                  <View style={styles.storecont} >
                    {getItems().stores.map((store) => {
                     return (
                   <StoreCardInList key={store.id} info={store} viewStore={this._viewStore} />
                    )
                     })}
                 </View>
              </ScrollView>

              <ScrollView vertical showsVerticalScrollIndicator={false} tabLabel="PRODUCTS">
							   <View style={styles.productscont}>
                  {getItems().products.map((list) => {
                return(
                   <ProductCard key={list.id} info={list} viewProduct={this._viewProduct} />
                   )})}
                 </View>
              </ScrollView>
              
						</ScrollableTabView>
				</View>

        </View>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

function mapStateToProps(state, ownProps) {
  return {
    cart: state.tstore.cart
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Brand);
