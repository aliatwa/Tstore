import React, { Component } from 'react';
import { Text, YellowBox } from 'react-native';

class Screen extends Component {
   constructor() {
 
    super();
  
    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
   ]);
  }

  render() {
    return (
      <Text>000</Text>
    );
  }
}

export default Screen;
