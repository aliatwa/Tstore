import React, { Component } from 'react';
import { Text, View, Alert, TextInput, Linking, ScrollView, YellowBox, ImageBackground, StyleSheet, Dimensions, Image, TouchableOpacity } from 'react-native';
import { colors } from '../configs/Colors.js';
import { strings } from '../locales/i18n';
import { bindActionCreators } from 'redux';
import StoreCardInList from '../components/StoreCardInList';
import { connect } from 'react-redux';
import * as Actions from '../reducres/tstore.actions';
import Icon from 'react-native-vector-icons/MaterialIcons';
const { width, height } = Dimensions.get('window');
import EStyleSheet from 'react-native-extended-stylesheet';
EStyleSheet.build({ $rem: width / 380 });


const navigatorStyle = {
  statusBarColor: colors.white,
  navBarTextFontFamily: 'Dosis-Medium',
  navBarTextFontSize: 30,
  statusBarTextColorScheme: 'dark',
  navigationBarColor: 'transparent',
  navBarTextColor: colors.primary,
  navBarButtonColor: colors.secondry,
  tabBarButtonColor: 'red',
  screenBackgroundColor: colors.white,
  tabBarSelectedButtonColor: 'red',
  tabBarBackgroundColor: 'white',
  topBarElevationShadowEnabled: false,
  navBarHideOnScroll: false,
  tabBarHidden: true,
  drawUnderTabBar: true,
  //drawUnderNavBar: true,
  //navBarTranslucent: true,
  navBarBackgroundColor: colors.white,
  //navBarHidden: false,
  //navBarBlur: false,
  //navBarTransparent: true,
  navBarTitleTextCentered: true
};

const styles = StyleSheet.create({
  container: {
    padding: 10,
    width: width,
    flexDirection: 'column',
    backgroundColor: colors.white,
    justifyContent: 'flex-start',
    alignItems: 'center'
  }

});
function getItems() {
  var json = {
    "stores": [{
      "id": 1,
      "title": "Giga",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 4,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1477120206578-46b3ca98a4e2?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=66315784054a15f1c2d79fc9ca929820&auto=format&fit=crop&w=334&q=80"
    },
    {
      "id": 2,
      "title": "Media",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1496250838193-02a942028e18?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=539b7735765bf84c978e6b9ad4133c59&auto=format&fit=crop&w=750&q=80"
    },
    {
      "id": 3,
      "title": "Omega",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1514792368985-f80e9d482a02?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b067b70aaa4390e9c1ad2133f406a377&auto=format&fit=crop&w=750&q=80"
    },
    {
      "id": 4,
      "title": "Memoa",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1445205170230-053b83016050?ixlib=rb-0.3.5&s=e5bd33f231c2af32a7bda0b4b070c1d3&auto=format&fit=crop&w=751&q=80"
    },
    {
      "id": 5,
      "title": "Tesla",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1441984443719-15c73b016ad1?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=63708ad8de717fa98f3bcfbbe506b283&auto=format&fit=crop&w=750&q=80"
    }],
  }
  return json;
}

class Stores extends Component {
  constructor(props) {
    super(props);
    this.state = { mapuri: null, distance: null, category: null, selectedbrand: null };
    YellowBox.ignoreWarnings([
      'Warning: componentWillMount is deprecated',
      'Warning: componentWillReceiveProps is deprecated',
    ]);
    this._nav = this._nav.bind(this);
    this._goToCart = this._goToCart.bind(this);
    this._viewStore = this._viewStore.bind(this);
  }

  componentDidMount() {
    //this.setState({  });
  }
  _viewStore(storeId) {
    this.props.navigator.showModal({
      screen: 'Store',
      navigatorStyle,
      passProps: {
        storeId
      },
      rightButtons: [{
        id: 'cart',
        component: 'CartCar',
        passProps: this._nav(),
      }],
      leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
      animated: true
    });
  }
  _nav() {
    var total = this.props.cart.reduce(function (_this, val) {
      return (_this + val.quantity)
    }, 0);
    return {
      quantity: total,
      getScreen: this._goToCart.bind(this)
    }
  }
  _goToCart() {
    console.log('cart');
    this.props.navigator.showModal({
      screen: 'Cart',
      navigatorStyle,
      leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
      animated: true
    });
  }

  render() {
    return (
    <View style={styles.container}>
      <ScrollView vertical showsVerticalScrollIndicator={false}>
              {getItems().stores.map((store) => {
                return (
                  <StoreCardInList key={store.id} info={store} viewStore={this._viewStore} />
                )
              })}  
      </ScrollView>
      </View>
    );
  }
}
function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

function mapStateToProps(state, ownProps) {
  return {
    cart: state.tstore.cart
  }
}
export default connect(mapStateToProps, mapDispatchToProps)(Stores);
