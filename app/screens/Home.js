import React, { Component } from 'react';
import { Text, YellowBox, TouchableOpacity, Animated, Image, Button, Easing, StyleSheet, Dimensions, View, ScrollView } from 'react-native';
import {colors} from '../configs/Colors.js';
const { width, height } = Dimensions.get('window');
import ProductCard from '../components/ProductCard';
import StoreCard from '../components/StoreCard';
import BrandCard from '../components/BrandCard';
import { bindActionCreators } from 'redux';
import { connect } from 'react-redux';
import * as Actions from '../reducres/tstore.actions';
import CategoryCard from '../components/CategoryCard';
import { strings } from '../locales/i18n';


const styles = StyleSheet.create({
  container: {
    flex: 1,
    //width: width,
    //height: height,
   // left: 200,
    justifyContent: 'flex-start',
    marginTop: 50,
    marginHorizontal: 10,
   // marginHorizontal: 40,
    //justifyContent: 'center',
   // alignItems: 'center',
    backgroundColor: colors.white,
  },
  heyEliza: {
    marginHorizontal: 40,
    fontFamily: "Dosis-Regular",
    fontSize: 28,
    fontWeight: "normal",
    letterSpacing: 0,
    textAlign: "left",
    color: colors.secondry
  },
  scroll:{
    //marginHorizontal: 10
  },
  Nearby:{
    marginVertical: 15,
  },
  category:{
    color: colors.secondry,
    fontFamily: "Dosis-Regular",
    fontSize: 28,
    textAlign: "left"
  },
  seeall:{
     color: colors.primary,
     fontFamily: "Dosis-Medium",
     fontSize: 17,
     textAlign: "right"
  },
  subtext:{
     flexDirection: 'row',
     alignItems: 'flex-end',
     marginBottom: 20,
     justifyContent: 'space-between'
  },
    discoverWinterMustHaves: {
    marginHorizontal: 40,
    marginTop: 20,
    fontFamily: "Dosis-Regular",
    fontSize: 55,
    textAlign: "left",
    color: colors.primary
 },
  listView: {
    flex: 1,
    justifyContent: 'center',
   },
  plusbrandcont: {
    backgroundColor: colors.primary,
    flexDirection: 'column',
    justifyContent: 'center',
    alignItems: 'center',
    height: 280,
    width: 200,
    borderRadius: 8,
    marginHorizontal: 5
  },
  plusbrand: {
    fontFamily: "Dosis-Medium",
    fontSize: 50,
    color: colors.white,
  }

});

const navigatorStyle = {
  statusBarColor: colors.white,
  navBarTextFontFamily: 'Dosis-Medium',
  navBarTextFontSize: 30,
  statusBarTextColorScheme: 'dark',
  navigationBarColor: 'transparent',
  navBarTextColor: colors.primary,
  navBarButtonColor: colors.secondry,
  tabBarButtonColor: 'red',
  screenBackgroundColor: colors.white,
  tabBarSelectedButtonColor: 'red',
  tabBarBackgroundColor: 'white',
  topBarElevationShadowEnabled: false,
  navBarHideOnScroll: false,
  tabBarHidden: true,
  drawUnderTabBar: true,
  //drawUnderNavBar: true,
  //navBarTranslucent: true,
  navBarBackgroundColor: colors.white,
  //navBarHidden: false,
  //navBarBlur: false,
  //navBarTransparent: true,
  navBarTitleTextCentered: true
};


function getItems() {
     var json =  {
       "products": [{
         "id": 1,
         "title": "Red shoe",
         "price": 120,
         "brandimg": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg",
         "image": "https://images.unsplash.com/photo-1505874462322-dfcf87f819a9?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b0b082569a3042da7bb5a6d8bf1c0f7d&auto=format&fit=crop&w=334&q=80"
       },
       {
         "id": 2,
         "title": "Handfree",
         "price": 334,
         "brandimg": "https://scontent-cai1-1.cdninstagram.com/vp/15694667eede1ccdda51dd094b523398/5BB50BD5/t51.2885-19/s150x150/28750886_600390463660226_2334580953218809856_n.jpg",
         "image": "https://images.unsplash.com/photo-1481207801830-97f0f9a1337e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=be08aede38d12b6e061a1c1a32cc173f&auto=format&fit=crop&w=750&q=80"
       },
       {
         "id": 3,
         "title": "Uber",
         "price": 675,
         "brandimg": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg",
         "image": "https://images.unsplash.com/photo-1521001750463-5f3e18f2da2d?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=819e9dcb75b84114278a7163ff38a91c&auto=format&fit=crop&w=334&q=80"
       },
       {
         "id": 4,
         "title": "Jug",
         "price": 90,
         "brandimg": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg",
         "image": "https://images.unsplash.com/photo-1511001148140-09c2b155f57c?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=5aadc8ce658d1a8996cdcb0a3144def5&auto=format&fit=crop&w=750&q=80"
       },
       {
         "id": 5,
         "title": "Tesla",
         "price": 90,
         "brandimg": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg",
         "image": "https://images.unsplash.com/photo-1522128483605-23607c944cbb?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=89c442b8ed5fd57c3a00e59113298a90&auto=format&fit=crop&w=749&q=80"
       }],
  "stores": [{
      "id": 1,
      "title": "Giga",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 4,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1477120206578-46b3ca98a4e2?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=66315784054a15f1c2d79fc9ca929820&auto=format&fit=crop&w=334&q=80"
    },
    {
      "id": 2,
      "title": "Media",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1496250838193-02a942028e18?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=539b7735765bf84c978e6b9ad4133c59&auto=format&fit=crop&w=750&q=80"
    },
    {
      "id": 3,
      "title": "Omega",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1514792368985-f80e9d482a02?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=b067b70aaa4390e9c1ad2133f406a377&auto=format&fit=crop&w=750&q=80"
    },
    {
      "id": 4,
      "title": "Memoa",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1445205170230-053b83016050?ixlib=rb-0.3.5&s=e5bd33f231c2af32a7bda0b4b070c1d3&auto=format&fit=crop&w=751&q=80"
    },
    {
      "id": 5,
      "title": "Tesla",
      "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
      "brands": [{
        "id": 1,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 2,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      },
      {
        "id": 3,
        "name": "Stabrq",
        "img": "https://scontent-cai1-1.cdninstagram.com/vp/eacefa37dea664a4abbef3c9679bd01d/5BB4D0AD/t51.2885-19/s150x150/14072910_1623145777977627_1848683726_a.jpg"
      }],
      "image": "https://images.unsplash.com/photo-1441984443719-15c73b016ad1?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=63708ad8de717fa98f3bcfbbe506b283&auto=format&fit=crop&w=750&q=80"
    }],
      "categories": [{ "id": 1,
                 "title": "Toys",
                 "image": "https://images.unsplash.com/photo-1484824823018-c36f00489002?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=45f6c16ed89b6c5b4ad7b4e06c26d0ff&auto=format&fit=crop&w=750&q=80"
               },
               { "id": 2,
                 "title": "Electronics",
                 "image": "https://images.unsplash.com/photo-1473831818960-c89731aabc3e?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=c5b723d05824021bdab67bc0e14a5fb0&auto=format&fit=crop&w=750&q=80"
               },
               { "id": 3,
                 "title": "Clothing",
                 "image": "https://images.unsplash.com/photo-1512436991641-6745cdb1723f?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=ad25f4eb5444edddb0c5fb252a7f1dce&auto=format&fit=crop&w=750&q=80"
               },
               { "id": 4,
                 "title": "Perfume",
                 "image": "https://images.unsplash.com/photo-1519493490532-9641938d51b2?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=e5c66943eed79118d407f475a5837bae&auto=format&fit=crop&w=750&q=80"
               },
               { "id": 5,
                 "title": "Gifts",
                 "image": "https://images.unsplash.com/photo-1453834190665-46ff0a1fbd5a?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=02d4a80ebaeb7a0a16734e34fe1aef58&auto=format&fit=crop&w=755&q=80"
               }],
        "brands": [{ "id": 1,
                 "title": "Stabrq",
                 "color": "#32cd32",
                 "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
                 "image": "https://scontent-cai1-1.xx.fbcdn.net/v/t1.0-1/p200x200/14063783_1083961178339966_3300795371085103026_n.png?_nc_cat=0&oh=4d28cab58053bfaaa7b56ad46a9932fa&oe=5B7A3BDD"
               },
               { "id": 2,
                 "title": "Goabas",
                 "color": "#5a1126",
                 "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
                 "image": "https://scontent-cai1-1.xx.fbcdn.net/v/t1.0-1/p200x200/29214836_1645169338885065_5874348580306681856_n.png?_nc_cat=0&oh=d91ccaec796e70c6cee7c3ff7c819cf6&oe=5BB4D4F4"
               },
               { "id": 3,
                 "title": "Khotwh",
                 "color": "#213c4f",
                 "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
                 "image": "https://scontent-cai1-1.xx.fbcdn.net/v/t1.0-1/p200x200/13094198_10154639415000639_4912758056589124617_n.png?_nc_cat=0&oh=e916eedaa0698229b891756caada4f78&oe=5BB95EA7"
               },
               { "id": 4,
                 "title": "IZO",
                 "color": "#010101",
                 "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
                 "image": "https://scontent-cai1-1.xx.fbcdn.net/v/t1.0-1/p200x200/22687951_539006209779229_4551073447838175917_n.jpg?_nc_cat=0&oh=03c419028a0db7d2c0fa8d4e8bbb1b04&oe=5BB90556"
               },
               { "id": 5,
                 "title": "Keswa",
                 "color": "#010214",
                 "categories": ['Kids', 'Toys', 'Clothing', 'Shoes'],
                 "image": "https://images.unsplash.com/photo-1453834190665-46ff0a1fbd5a?ixlib=rb-0.3.5&ixid=eyJhcHBfaWQiOjEyMDd9&s=02d4a80ebaeb7a0a16734e34fe1aef58&auto=format&fit=crop&w=755&q=80"
               }]
             };
  return json;
   }

class Home extends Component {
   constructor(props) {
    super(props);
    Text.defaultProps.style = { fontFamily: 'Dosis-Regular' }
    YellowBox.ignoreWarnings([
     'Warning: componentWillMount is deprecated',
     'Warning: componentWillReceiveProps is deprecated',
     'Warning: componentWillReceiveUpdate is deprecated',
     'Warning: componentDidUpdate is deprecated',
     'Warning: componentWillUpdate is deprecated',
   ]);
      
    this._viewProduct = this._viewProduct.bind(this);
    this._viewStore = this._viewStore.bind(this);
    this._viewBrand = this._viewBrand.bind(this);
    this._viewCategory = this._viewCategory.bind(this);
    this._goToCart = this._goToCart.bind(this);
    this._onLayoutEvent = this._onLayoutEvent.bind(this);
    this.currentPosition = this.currentPosition.bind(this);
    this.openCategories = this.openCategories.bind(this);
    this.openStores = this.openStores.bind(this);
    this.openBrands = this.openBrands.bind(this);
    this.props.navigator.setOnNavigatorEvent(this.onNavigatorEvent.bind(this));
    this.props.navigator.setButtons(this.navigatorButtons(this.props.navigator));
  }


componentDidMount() {
  //setTimeout(() => this._scrollView.getScrollResponder().scrollTo({x: 0, y: 0, animated: true}) , 0);
 // this.currentPosition();
 const monthNames = ["January", "February", "March", "April", "May", "June",
  "July", "August", "September", "October", "November", "December"];
 const date = monthNames[new Date().getMonth()]+' '+ new Date().getDate()+', '+new Date().getFullYear();
 console.log(date)
}


navigatorButtons = (navigator) => {
    return {
      rightButtons: [
        {
          id: 'cart',
          component: 'CartCar',
          passProps: this._nav(),
        }
      ]
    };
  }

_nav() {
  var total = this.props.cart.reduce(function(_this, val) {
          return (_this+val.quantity)
      }, 0);
    return {
        quantity: total,
        getScreen: this._goToCart.bind(this)
    }
}
onNavigatorEvent(event) { // this is the onPress handler for the two buttons together
   //console.log(event);
   if (event.type == 'NavBarButtonPress') { // this is the event type for button presses
      if (event.id == 'cart') { // this is the same id field from the static navigatorButtons definition
        this._goToCart()
        //console.log('cart');
      }
    }
  }

_viewProduct(productId) {
   this.props.navigator.showModal({
      screen: 'ProductDetails',
      navigatorStyle,
      passProps: {
        productId
      },
        rightButtons: [{
        id: 'cart',
        component: 'CartCar',
        passProps: this._nav(),    
            }],
        leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
      animated: true
    });
}

_viewStore(StoreId) {
     this.props.navigator.showModal({
      screen: 'Store',
      navigatorStyle,
      title: 'Story',
      passProps: {
        StoreId
      },
       rightButtons: [{
         id: 'cart',
         component: 'CartCar',
         passProps: this._nav(),
       }],
       leftButtons: [{
         id: 'back',
         icon: require('../images/left.png')
       }],
    });
}
_viewBrand(brandId) {
     this.props.navigator.showModal({
      screen: 'Brand',
      navigatorStyle,
      title: 'Brand',
      passProps: {
        brandId
      },
       rightButtons: [{
         id: 'cart',
         component: 'CartCar',
         passProps: this._nav(),
       }],
       leftButtons: [{
         id: 'back',
         icon: require('../images/left.png')
       }],
    });
}
openCategories() {
     this.props.navigator.showModal({
      screen: 'Category',
      animated: true,
      navigatorStyle,
      title: 'Categories',
      passProps: {
      },
      rightButtons: [{
        id: 'cart',
        component: 'CartCar',
        passProps: this._nav(),    
            }],
        leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
    });
}

openStores() {
     this.props.navigator.showModal({
      screen: 'Stores',
      animated: true,
      navigatorStyle,
      title: 'Stores',
      passProps: {
      },
      rightButtons: [{
        id: 'cart',
        component: 'CartCar',
        passProps: this._nav(),    
            }],
        leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
    });
}
openBrands() {
     this.props.navigator.showModal({
      screen: 'Brands',
      animated: true,
      navigatorStyle,
      title: 'Brands',
      passProps: {
      },
      rightButtons: [{
        id: 'cart',
        component: 'CartCar',
        passProps: this._nav(),    
            }],
        leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
    });
}

currentPosition() {
 navigator.geolocation.getCurrentPosition(
       (position) => {
         this.setState({
           latitude: position.coords.latitude,
           longitude: position.coords.longitude,
           error: null,
         });
       },
       (error) => this.setState({ error: error.message }),
       { enableHighAccuracy: true, timeout: 200000, maximumAge: 1000 },
     );

 }


_goToCart() {
  console.log('cart');
     this.props.navigator.showModal({
      screen: 'Cart',
      navigatorStyle,
      leftButtons: [{
        id: 'back',
        icon: require('../images/left.png')
      }],
      animated: true
    });
}
_viewCategory(CategoryId) {
     this.props.navigator.showModal({
      screen: 'ProductDetails',
      passProps: {
        CategoryId
      },
    });
  }
_onLayoutEvent(event) {
     this.setState({componentHeight: event.nativeEvent.layout.height});
}

render() {
    const items = getItems();
    return (
      <ScrollView vertical showsVerticalScrollIndicator={false}>
      <View onLayout={(event) => this._onLayoutEvent(event)} style={[styles.container]} >
       <Text style={styles.heyEliza}>{strings('home.hey', { name: 'Hassan' })}</Text>
       <Text style={styles.discoverWinterMustHaves} adjustsFontSizeToFit numberOfLines={2}>
       {strings('home.welcome')}</Text> 

      

      <View style={styles.Nearby}>
      <View style={styles.subtext}>
      <Text style={styles.category}>Nearby Stores</Text> 
           <TouchableOpacity activeOpacity={0.8} onPress={this.openStores.bind(this)}>
              <Text style={styles.seeall}>SEE ALL</Text>
           </TouchableOpacity>
      </View>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
       {items.stores.map((listStore) => {
        return (
            <StoreCard key={listStore.id} info={listStore} viewStore={this._viewStore} />
            )})}
      </ScrollView>
      </View>


      <View style={styles.Nearby}>
      <View style={styles.subtext}>
      <Text style={styles.category}>Shop by Brands</Text> 
           <TouchableOpacity activeOpacity={0.8} onPress={this.openBrands.bind(this)}>
              <Text style={styles.seeall}>SEE ALL</Text>
           </TouchableOpacity>
      </View>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
       {items.brands.slice(0,3).map((brand) => {
        return (
            <BrandCard style={{marginRight: 25}} key={brand.id} info={brand} viewBrand={this._viewBrand} />
            )})}
       {items.brands.length > 3 &&
        <TouchableOpacity activeOpacity={0.8} onPress={this.openBrands.bind(this)}>
       <View style={styles.plusbrandcont}>
            <Text style={styles.plusbrand} numberOfLines={1}>+3</Text>
       </View>
       </TouchableOpacity>
      } 
      </ScrollView>
      </View>



      {/*<View style={styles.Nearby}>
      <View style={styles.subtext}>
      <Text style={styles.category}>You may like</Text> 
      </View>
      <ScrollView style={styles.scroll} ref={(view) => this._scrollView = view} horizontal showsHorizontalScrollIndicator={false}>
       {items.products.map((list) => {
        return (
            <ProductCard key={list.id} info={list} viewProduct={this._viewProduct} />
            )})}
      </ScrollView>
          </View>*/}

      <View style={styles.Nearby}>
        <View style={styles.subtext}>
           <Text style={styles.category}>Shop by Category</Text> 
           <TouchableOpacity activeOpacity={0.8} onPress={this.openCategories.bind(this)}>
              <Text style={styles.seeall}>SEE ALL</Text>
           </TouchableOpacity>
        </View>
      <ScrollView horizontal showsHorizontalScrollIndicator={false}>
       {items.categories.map((listCategory) => {
        return (
            <CategoryCard key={listCategory.id} info={listCategory} viewCategory={this._viewCategory} />
            )})}
      </ScrollView>
      </View>

      
      </View>
      </ScrollView>
          );
  }
}

function mapDispatchToProps(dispatch) {
  return {
    actions: bindActionCreators(Actions, dispatch)
  };
}

function mapStateToProps(state, ownProps) {
  return {
    cart: state.tstore.cart
  }
}
export default connect(mapStateToProps, mapDispatchToProps) (Home);
